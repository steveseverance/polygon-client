
# flake8: noqa

# Import all APIs into this package.
# If you have many APIs here with many many models used in each API this may
# raise a `RecursionError`.
# In order to avoid this, import only the API that you directly need like:
#
#   from .api.crypto_api import CryptoApi
#
# or import this package, but before doing it, use:
#
#   import sys
#   sys.setrecursionlimit(n)

# Import APIs into API package:
from openapi_client.api.crypto_api import CryptoApi
from openapi_client.api.currencies___crypto_api import CurrenciesCryptoApi
from openapi_client.api.currencies___forex_api import CurrenciesForexApi
from openapi_client.api.reference_api import ReferenceApi
from openapi_client.api.stocks___equities_api import StocksEquitiesApi
