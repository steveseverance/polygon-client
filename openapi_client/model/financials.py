"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from openapi_client.model_utils import (  # noqa: F401
    ApiTypeError,
    ModelComposed,
    ModelNormal,
    ModelSimple,
    cached_property,
    change_keys_js_to_python,
    convert_js_args_to_python_args,
    date,
    datetime,
    file_type,
    none_type,
    validate_get_composed_info,
)


class Financials(ModelNormal):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.

    Attributes:
      allowed_values (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          with a capitalized key describing the allowed value and an allowed
          value. These dicts store the allowed enum values.
      attribute_map (dict): The key is attribute name
          and the value is json key in definition.
      discriminator_value_class_map (dict): A dict to go from the discriminator
          variable value to the discriminator class name.
      validations (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          that stores validations for max_length, min_length, max_items,
          min_items, exclusive_maximum, inclusive_maximum, exclusive_minimum,
          inclusive_minimum, and regex.
      additional_properties_type (tuple): A tuple of classes accepted
          as additional properties values.
    """

    allowed_values = {
        ('period',): {
            'Q': "Q",
            'T': "T",
            'QA': "QA",
            'TA': "TA",
            'Y': "Y",
            'YA': "YA",
        },
    }

    validations = {
    }

    additional_properties_type = None

    _nullable = False

    @cached_property
    def openapi_types():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded

        Returns
            openapi_types (dict): The key is attribute name
                and the value is attribute type.
        """
        return {
            'ticker': (str,),  # noqa: E501
            'period': (str,),  # noqa: E501
            'calendar_date': (datetime,),  # noqa: E501
            'report_period': (datetime,),  # noqa: E501
            'updated': (datetime,),  # noqa: E501
            'accumulated_other_comprehensive_income': (int,),  # noqa: E501
            'assets': (int,),  # noqa: E501
            'assets_average': (int,),  # noqa: E501
            'assets_current': (int,),  # noqa: E501
            'asset_turnover': (int,),  # noqa: E501
            'assets_non_current': (int,),  # noqa: E501
            'book_value_per_share': (float,),  # noqa: E501
            'capital_expenditure': (int,),  # noqa: E501
            'cash_and_equivalents': (int,),  # noqa: E501
            'cash_and_equivalents_usd': (int,),  # noqa: E501
            'cost_of_revenue': (int,),  # noqa: E501
            'consolidated_income': (int,),  # noqa: E501
            'current_ratio': (float,),  # noqa: E501
            'debt_to_equity_ratio': (float,),  # noqa: E501
            'debt': (int,),  # noqa: E501
            'debt_current': (int,),  # noqa: E501
            'debt_non_current': (int,),  # noqa: E501
            'debt_usd': (int,),  # noqa: E501
            'deferred_revenue': (int,),  # noqa: E501
            'depreciation_amortization_and_accretion': (int,),  # noqa: E501
            'deposits': (int,),  # noqa: E501
            'dividend_yield': (int,),  # noqa: E501
            'dividends_per_basic_common_share': (int,),  # noqa: E501
            'earning_before_interest_taxes': (int,),  # noqa: E501
            'earnings_before_interest_taxes_depreciation_amortization': (int,),  # noqa: E501
            'ebitda_margin': (float,),  # noqa: E501
            'earnings_before_interest_taxes_depreciation_amortization_usd': (int,),  # noqa: E501
            'earning_before_interest_taxes_usd': (int,),  # noqa: E501
            'earnings_before_tax': (int,),  # noqa: E501
            'earnings_per_basic_share': (float,),  # noqa: E501
            'earnings_per_diluted_share': (float,),  # noqa: E501
            'earnings_per_basic_share_usd': (float,),  # noqa: E501
            'shareholders_equity': (int,),  # noqa: E501
            'average_equity': (int,),  # noqa: E501
            'shareholders_equity_usd': (int,),  # noqa: E501
            'enterprise_value': (int,),  # noqa: E501
            'enterprise_value_over_ebit': (int,),  # noqa: E501
            'enterprise_value_over_ebitda': (float,),  # noqa: E501
            'free_cash_flow': (int,),  # noqa: E501
            'free_cash_flow_per_share': (float,),  # noqa: E501
            'foreign_currency_usd_exchange_rate': (int,),  # noqa: E501
            'gross_profit': (int,),  # noqa: E501
            'gross_margin': (float,),  # noqa: E501
            'goodwill_and_intangible_assets': (int,),  # noqa: E501
            'interest_expense': (int,),  # noqa: E501
            'invested_capital': (int,),  # noqa: E501
            'invested_capital_average': (int,),  # noqa: E501
            'inventory': (int,),  # noqa: E501
            'investments': (int,),  # noqa: E501
            'investments_current': (int,),  # noqa: E501
            'investments_non_current': (int,),  # noqa: E501
            'total_liabilities': (int,),  # noqa: E501
            'current_liabilities': (int,),  # noqa: E501
            'liabilities_non_current': (int,),  # noqa: E501
            'market_capitalization': (int,),  # noqa: E501
            'net_cash_flow': (int,),  # noqa: E501
            'net_cash_flow_business_acquisitions_disposals': (int,),  # noqa: E501
            'issuance_equity_shares': (int,),  # noqa: E501
            'issuance_debt_securities': (int,),  # noqa: E501
            'payment_dividends_other_cash_distributions': (int,),  # noqa: E501
            'net_cash_flow_from_financing': (int,),  # noqa: E501
            'net_cash_flow_from_investing': (int,),  # noqa: E501
            'net_cash_flow_investment_acquisitions_disposals': (int,),  # noqa: E501
            'net_cash_flow_from_operations': (int,),  # noqa: E501
            'effect_of_exchange_rate_changes_on_cash': (int,),  # noqa: E501
            'net_income': (int,),  # noqa: E501
            'net_income_common_stock': (int,),  # noqa: E501
            'net_income_common_stock_usd': (int,),  # noqa: E501
            'net_loss_income_from_discontinued_operations': (int,),  # noqa: E501
            'net_income_to_non_controlling_interests': (int,),  # noqa: E501
            'profit_margin': (float,),  # noqa: E501
            'operating_expenses': (int,),  # noqa: E501
            'operating_income': (int,),  # noqa: E501
            'trade_and_non_trade_payables': (int,),  # noqa: E501
            'payout_ratio': (int,),  # noqa: E501
            'price_to_book_value': (float,),  # noqa: E501
            'price_earnings': (float,),  # noqa: E501
            'price_to_earnings_ratio': (float,),  # noqa: E501
            'property_plant_equipment_net': (int,),  # noqa: E501
            'preferred_dividends_income_statement_impact': (int,),  # noqa: E501
            'share_price_adjusted_close': (float,),  # noqa: E501
            'price_sales': (float,),  # noqa: E501
            'price_to_sales_ratio': (float,),  # noqa: E501
            'trade_and_non_trade_receivables': (int,),  # noqa: E501
            'accumulated_retained_earnings_deficit': (int,),  # noqa: E501
            'revenues': (int,),  # noqa: E501
            'revenues_usd': (int,),  # noqa: E501
            'research_and_development_expense': (int,),  # noqa: E501
            'return_on_average_assets': (int,),  # noqa: E501
            'return_on_average_equity': (int,),  # noqa: E501
            'return_on_invested_capital': (int,),  # noqa: E501
            'return_on_sales': (float,),  # noqa: E501
            'share_based_compensation': (int,),  # noqa: E501
            'selling_general_and_administrative_expense': (int,),  # noqa: E501
            'share_factor': (int,),  # noqa: E501
            'shares': (int,),  # noqa: E501
            'weighted_average_shares': (int,),  # noqa: E501
            'weighted_average_shares_diluted': (int,),  # noqa: E501
            'sales_per_share': (float,),  # noqa: E501
            'tangible_asset_value': (int,),  # noqa: E501
            'tax_assets': (int,),  # noqa: E501
            'income_tax_expense': (int,),  # noqa: E501
            'tax_liabilities': (int,),  # noqa: E501
            'tangible_assets_book_value_per_share': (float,),  # noqa: E501
            'working_capital': (int,),  # noqa: E501
        }

    @cached_property
    def discriminator():
        return None


    attribute_map = {
        'ticker': 'ticker',  # noqa: E501
        'period': 'period',  # noqa: E501
        'calendar_date': 'calendarDate',  # noqa: E501
        'report_period': 'reportPeriod',  # noqa: E501
        'updated': 'updated',  # noqa: E501
        'accumulated_other_comprehensive_income': 'accumulatedOtherComprehensiveIncome',  # noqa: E501
        'assets': 'assets',  # noqa: E501
        'assets_average': 'assetsAverage',  # noqa: E501
        'assets_current': 'assetsCurrent',  # noqa: E501
        'asset_turnover': 'assetTurnover',  # noqa: E501
        'assets_non_current': 'assetsNonCurrent',  # noqa: E501
        'book_value_per_share': 'bookValuePerShare',  # noqa: E501
        'capital_expenditure': 'capitalExpenditure',  # noqa: E501
        'cash_and_equivalents': 'cashAndEquivalents',  # noqa: E501
        'cash_and_equivalents_usd': 'cashAndEquivalentsUSD',  # noqa: E501
        'cost_of_revenue': 'costOfRevenue',  # noqa: E501
        'consolidated_income': 'consolidatedIncome',  # noqa: E501
        'current_ratio': 'currentRatio',  # noqa: E501
        'debt_to_equity_ratio': 'debtToEquityRatio',  # noqa: E501
        'debt': 'debt',  # noqa: E501
        'debt_current': 'debtCurrent',  # noqa: E501
        'debt_non_current': 'debtNonCurrent',  # noqa: E501
        'debt_usd': 'debtUSD',  # noqa: E501
        'deferred_revenue': 'deferredRevenue',  # noqa: E501
        'depreciation_amortization_and_accretion': 'depreciationAmortizationAndAccretion',  # noqa: E501
        'deposits': 'deposits',  # noqa: E501
        'dividend_yield': 'dividendYield',  # noqa: E501
        'dividends_per_basic_common_share': 'dividendsPerBasicCommonShare',  # noqa: E501
        'earning_before_interest_taxes': 'earningBeforeInterestTaxes',  # noqa: E501
        'earnings_before_interest_taxes_depreciation_amortization': 'earningsBeforeInterestTaxesDepreciationAmortization',  # noqa: E501
        'ebitda_margin': 'EBITDAMargin',  # noqa: E501
        'earnings_before_interest_taxes_depreciation_amortization_usd': 'earningsBeforeInterestTaxesDepreciationAmortizationUSD',  # noqa: E501
        'earning_before_interest_taxes_usd': 'earningBeforeInterestTaxesUSD',  # noqa: E501
        'earnings_before_tax': 'earningsBeforeTax',  # noqa: E501
        'earnings_per_basic_share': 'earningsPerBasicShare',  # noqa: E501
        'earnings_per_diluted_share': 'earningsPerDilutedShare',  # noqa: E501
        'earnings_per_basic_share_usd': 'earningsPerBasicShareUSD',  # noqa: E501
        'shareholders_equity': 'shareholdersEquity',  # noqa: E501
        'average_equity': 'averageEquity',  # noqa: E501
        'shareholders_equity_usd': 'shareholdersEquityUSD',  # noqa: E501
        'enterprise_value': 'enterpriseValue',  # noqa: E501
        'enterprise_value_over_ebit': 'enterpriseValueOverEBIT',  # noqa: E501
        'enterprise_value_over_ebitda': 'enterpriseValueOverEBITDA',  # noqa: E501
        'free_cash_flow': 'freeCashFlow',  # noqa: E501
        'free_cash_flow_per_share': 'freeCashFlowPerShare',  # noqa: E501
        'foreign_currency_usd_exchange_rate': 'foreignCurrencyUSDExchangeRate',  # noqa: E501
        'gross_profit': 'grossProfit',  # noqa: E501
        'gross_margin': 'grossMargin',  # noqa: E501
        'goodwill_and_intangible_assets': 'goodwillAndIntangibleAssets',  # noqa: E501
        'interest_expense': 'interestExpense',  # noqa: E501
        'invested_capital': 'investedCapital',  # noqa: E501
        'invested_capital_average': 'investedCapitalAverage',  # noqa: E501
        'inventory': 'inventory',  # noqa: E501
        'investments': 'investments',  # noqa: E501
        'investments_current': 'investmentsCurrent',  # noqa: E501
        'investments_non_current': 'investmentsNonCurrent',  # noqa: E501
        'total_liabilities': 'totalLiabilities',  # noqa: E501
        'current_liabilities': 'currentLiabilities',  # noqa: E501
        'liabilities_non_current': 'liabilitiesNonCurrent',  # noqa: E501
        'market_capitalization': 'marketCapitalization',  # noqa: E501
        'net_cash_flow': 'netCashFlow',  # noqa: E501
        'net_cash_flow_business_acquisitions_disposals': 'netCashFlowBusinessAcquisitionsDisposals',  # noqa: E501
        'issuance_equity_shares': 'issuanceEquityShares',  # noqa: E501
        'issuance_debt_securities': 'issuanceDebtSecurities',  # noqa: E501
        'payment_dividends_other_cash_distributions': 'paymentDividendsOtherCashDistributions',  # noqa: E501
        'net_cash_flow_from_financing': 'netCashFlowFromFinancing',  # noqa: E501
        'net_cash_flow_from_investing': 'netCashFlowFromInvesting',  # noqa: E501
        'net_cash_flow_investment_acquisitions_disposals': 'netCashFlowInvestmentAcquisitionsDisposals',  # noqa: E501
        'net_cash_flow_from_operations': 'netCashFlowFromOperations',  # noqa: E501
        'effect_of_exchange_rate_changes_on_cash': 'effectOfExchangeRateChangesOnCash',  # noqa: E501
        'net_income': 'netIncome',  # noqa: E501
        'net_income_common_stock': 'netIncomeCommonStock',  # noqa: E501
        'net_income_common_stock_usd': 'netIncomeCommonStockUSD',  # noqa: E501
        'net_loss_income_from_discontinued_operations': 'netLossIncomeFromDiscontinuedOperations',  # noqa: E501
        'net_income_to_non_controlling_interests': 'netIncomeToNonControllingInterests',  # noqa: E501
        'profit_margin': 'profitMargin',  # noqa: E501
        'operating_expenses': 'operatingExpenses',  # noqa: E501
        'operating_income': 'operatingIncome',  # noqa: E501
        'trade_and_non_trade_payables': 'tradeAndNonTradePayables',  # noqa: E501
        'payout_ratio': 'payoutRatio',  # noqa: E501
        'price_to_book_value': 'priceToBookValue',  # noqa: E501
        'price_earnings': 'priceEarnings',  # noqa: E501
        'price_to_earnings_ratio': 'priceToEarningsRatio',  # noqa: E501
        'property_plant_equipment_net': 'propertyPlantEquipmentNet',  # noqa: E501
        'preferred_dividends_income_statement_impact': 'preferredDividendsIncomeStatementImpact',  # noqa: E501
        'share_price_adjusted_close': 'sharePriceAdjustedClose',  # noqa: E501
        'price_sales': 'priceSales',  # noqa: E501
        'price_to_sales_ratio': 'priceToSalesRatio',  # noqa: E501
        'trade_and_non_trade_receivables': 'tradeAndNonTradeReceivables',  # noqa: E501
        'accumulated_retained_earnings_deficit': 'accumulatedRetainedEarningsDeficit',  # noqa: E501
        'revenues': 'revenues',  # noqa: E501
        'revenues_usd': 'revenuesUSD',  # noqa: E501
        'research_and_development_expense': 'researchAndDevelopmentExpense',  # noqa: E501
        'return_on_average_assets': 'returnOnAverageAssets',  # noqa: E501
        'return_on_average_equity': 'returnOnAverageEquity',  # noqa: E501
        'return_on_invested_capital': 'returnOnInvestedCapital',  # noqa: E501
        'return_on_sales': 'returnOnSales',  # noqa: E501
        'share_based_compensation': 'shareBasedCompensation',  # noqa: E501
        'selling_general_and_administrative_expense': 'sellingGeneralAndAdministrativeExpense',  # noqa: E501
        'share_factor': 'shareFactor',  # noqa: E501
        'shares': 'shares',  # noqa: E501
        'weighted_average_shares': 'weightedAverageShares',  # noqa: E501
        'weighted_average_shares_diluted': 'weightedAverageSharesDiluted',  # noqa: E501
        'sales_per_share': 'salesPerShare',  # noqa: E501
        'tangible_asset_value': 'tangibleAssetValue',  # noqa: E501
        'tax_assets': 'taxAssets',  # noqa: E501
        'income_tax_expense': 'incomeTaxExpense',  # noqa: E501
        'tax_liabilities': 'taxLiabilities',  # noqa: E501
        'tangible_assets_book_value_per_share': 'tangibleAssetsBookValuePerShare',  # noqa: E501
        'working_capital': 'workingCapital',  # noqa: E501
    }

    _composed_schemas = {}

    required_properties = set([
        '_data_store',
        '_check_type',
        '_spec_property_naming',
        '_path_to_item',
        '_configuration',
        '_visited_composed_classes',
    ])

    @convert_js_args_to_python_args
    def __init__(self, ticker, *args, **kwargs):  # noqa: E501
        """Financials - a model defined in OpenAPI

        Args:
            ticker (str): The exchange symbol that this item is traded under.

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            period (str): Reporting period.. [optional]  # noqa: E501
            calendar_date (datetime): [optional]  # noqa: E501
            report_period (datetime): [optional]  # noqa: E501
            updated (datetime): [optional]  # noqa: E501
            accumulated_other_comprehensive_income (int): [optional]  # noqa: E501
            assets (int): [optional]  # noqa: E501
            assets_average (int): [optional]  # noqa: E501
            assets_current (int): [optional]  # noqa: E501
            asset_turnover (int): [optional]  # noqa: E501
            assets_non_current (int): [optional]  # noqa: E501
            book_value_per_share (float): [optional]  # noqa: E501
            capital_expenditure (int): [optional]  # noqa: E501
            cash_and_equivalents (int): [optional]  # noqa: E501
            cash_and_equivalents_usd (int): [optional]  # noqa: E501
            cost_of_revenue (int): [optional]  # noqa: E501
            consolidated_income (int): [optional]  # noqa: E501
            current_ratio (float): [optional]  # noqa: E501
            debt_to_equity_ratio (float): [optional]  # noqa: E501
            debt (int): [optional]  # noqa: E501
            debt_current (int): [optional]  # noqa: E501
            debt_non_current (int): [optional]  # noqa: E501
            debt_usd (int): [optional]  # noqa: E501
            deferred_revenue (int): [optional]  # noqa: E501
            depreciation_amortization_and_accretion (int): [optional]  # noqa: E501
            deposits (int): [optional]  # noqa: E501
            dividend_yield (int): [optional]  # noqa: E501
            dividends_per_basic_common_share (int): [optional]  # noqa: E501
            earning_before_interest_taxes (int): [optional]  # noqa: E501
            earnings_before_interest_taxes_depreciation_amortization (int): [optional]  # noqa: E501
            ebitda_margin (float): [optional]  # noqa: E501
            earnings_before_interest_taxes_depreciation_amortization_usd (int): [optional]  # noqa: E501
            earning_before_interest_taxes_usd (int): [optional]  # noqa: E501
            earnings_before_tax (int): [optional]  # noqa: E501
            earnings_per_basic_share (float): [optional]  # noqa: E501
            earnings_per_diluted_share (float): [optional]  # noqa: E501
            earnings_per_basic_share_usd (float): [optional]  # noqa: E501
            shareholders_equity (int): [optional]  # noqa: E501
            average_equity (int): [optional]  # noqa: E501
            shareholders_equity_usd (int): [optional]  # noqa: E501
            enterprise_value (int): [optional]  # noqa: E501
            enterprise_value_over_ebit (int): [optional]  # noqa: E501
            enterprise_value_over_ebitda (float): [optional]  # noqa: E501
            free_cash_flow (int): [optional]  # noqa: E501
            free_cash_flow_per_share (float): [optional]  # noqa: E501
            foreign_currency_usd_exchange_rate (int): [optional]  # noqa: E501
            gross_profit (int): [optional]  # noqa: E501
            gross_margin (float): [optional]  # noqa: E501
            goodwill_and_intangible_assets (int): [optional]  # noqa: E501
            interest_expense (int): [optional]  # noqa: E501
            invested_capital (int): [optional]  # noqa: E501
            invested_capital_average (int): [optional]  # noqa: E501
            inventory (int): [optional]  # noqa: E501
            investments (int): [optional]  # noqa: E501
            investments_current (int): [optional]  # noqa: E501
            investments_non_current (int): [optional]  # noqa: E501
            total_liabilities (int): [optional]  # noqa: E501
            current_liabilities (int): [optional]  # noqa: E501
            liabilities_non_current (int): [optional]  # noqa: E501
            market_capitalization (int): [optional]  # noqa: E501
            net_cash_flow (int): [optional]  # noqa: E501
            net_cash_flow_business_acquisitions_disposals (int): [optional]  # noqa: E501
            issuance_equity_shares (int): [optional]  # noqa: E501
            issuance_debt_securities (int): [optional]  # noqa: E501
            payment_dividends_other_cash_distributions (int): [optional]  # noqa: E501
            net_cash_flow_from_financing (int): [optional]  # noqa: E501
            net_cash_flow_from_investing (int): [optional]  # noqa: E501
            net_cash_flow_investment_acquisitions_disposals (int): [optional]  # noqa: E501
            net_cash_flow_from_operations (int): [optional]  # noqa: E501
            effect_of_exchange_rate_changes_on_cash (int): [optional]  # noqa: E501
            net_income (int): [optional]  # noqa: E501
            net_income_common_stock (int): [optional]  # noqa: E501
            net_income_common_stock_usd (int): [optional]  # noqa: E501
            net_loss_income_from_discontinued_operations (int): [optional]  # noqa: E501
            net_income_to_non_controlling_interests (int): [optional]  # noqa: E501
            profit_margin (float): [optional]  # noqa: E501
            operating_expenses (int): [optional]  # noqa: E501
            operating_income (int): [optional]  # noqa: E501
            trade_and_non_trade_payables (int): [optional]  # noqa: E501
            payout_ratio (int): [optional]  # noqa: E501
            price_to_book_value (float): [optional]  # noqa: E501
            price_earnings (float): [optional]  # noqa: E501
            price_to_earnings_ratio (float): [optional]  # noqa: E501
            property_plant_equipment_net (int): [optional]  # noqa: E501
            preferred_dividends_income_statement_impact (int): [optional]  # noqa: E501
            share_price_adjusted_close (float): [optional]  # noqa: E501
            price_sales (float): [optional]  # noqa: E501
            price_to_sales_ratio (float): [optional]  # noqa: E501
            trade_and_non_trade_receivables (int): [optional]  # noqa: E501
            accumulated_retained_earnings_deficit (int): [optional]  # noqa: E501
            revenues (int): [optional]  # noqa: E501
            revenues_usd (int): [optional]  # noqa: E501
            research_and_development_expense (int): [optional]  # noqa: E501
            return_on_average_assets (int): [optional]  # noqa: E501
            return_on_average_equity (int): [optional]  # noqa: E501
            return_on_invested_capital (int): [optional]  # noqa: E501
            return_on_sales (float): [optional]  # noqa: E501
            share_based_compensation (int): [optional]  # noqa: E501
            selling_general_and_administrative_expense (int): [optional]  # noqa: E501
            share_factor (int): [optional]  # noqa: E501
            shares (int): [optional]  # noqa: E501
            weighted_average_shares (int): [optional]  # noqa: E501
            weighted_average_shares_diluted (int): [optional]  # noqa: E501
            sales_per_share (float): [optional]  # noqa: E501
            tangible_asset_value (int): [optional]  # noqa: E501
            tax_assets (int): [optional]  # noqa: E501
            income_tax_expense (int): [optional]  # noqa: E501
            tax_liabilities (int): [optional]  # noqa: E501
            tangible_assets_book_value_per_share (float): [optional]  # noqa: E501
            working_capital (int): [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', False)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        if args:
            raise ApiTypeError(
                "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                    args,
                    self.__class__.__name__,
                ),
                path_to_item=_path_to_item,
                valid_classes=(self.__class__,),
            )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        self.ticker = ticker
        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
