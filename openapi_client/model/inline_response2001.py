"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from openapi_client.model_utils import (  # noqa: F401
    ApiTypeError,
    ModelComposed,
    ModelNormal,
    ModelSimple,
    cached_property,
    change_keys_js_to_python,
    convert_js_args_to_python_args,
    date,
    datetime,
    file_type,
    none_type,
    validate_get_composed_info,
)


class InlineResponse2001(ModelNormal):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.

    Attributes:
      allowed_values (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          with a capitalized key describing the allowed value and an allowed
          value. These dicts store the allowed enum values.
      attribute_map (dict): The key is attribute name
          and the value is json key in definition.
      discriminator_value_class_map (dict): A dict to go from the discriminator
          variable value to the discriminator class name.
      validations (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          that stores validations for max_length, min_length, max_items,
          min_items, exclusive_maximum, inclusive_maximum, exclusive_minimum,
          inclusive_minimum, and regex.
      additional_properties_type (tuple): A tuple of classes accepted
          as additional properties values.
    """

    allowed_values = {
    }

    validations = {
    }

    additional_properties_type = None

    _nullable = False

    @cached_property
    def openapi_types():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded

        Returns
            openapi_types (dict): The key is attribute name
                and the value is attribute type.
        """
        return {
            'logo': (str,),  # noqa: E501
            'exchange': (str,),  # noqa: E501
            'exchange_symbol': (str,),  # noqa: E501
            'type': (str,),  # noqa: E501
            'name': (str,),  # noqa: E501
            'symbol': (str,),  # noqa: E501
            'listdate': (date,),  # noqa: E501
            'cik': (str,),  # noqa: E501
            'bloomberg': (str,),  # noqa: E501
            'figi': (str,),  # noqa: E501
            'lei': (str,),  # noqa: E501
            'sic': (int,),  # noqa: E501
            'country': (str,),  # noqa: E501
            'industry': (str,),  # noqa: E501
            'sector': (str,),  # noqa: E501
            'marketcap': (int,),  # noqa: E501
            'employees': (int,),  # noqa: E501
            'phone': (str,),  # noqa: E501
            'ceo': (str,),  # noqa: E501
            'url': (str,),  # noqa: E501
            'description': (str,),  # noqa: E501
            'hq_address': (str,),  # noqa: E501
            'hq_state': (str,),  # noqa: E501
            'hq_country': (str,),  # noqa: E501
            'similar': ([str],),  # noqa: E501
            'tags': ([str],),  # noqa: E501
            'updated': (date,),  # noqa: E501
            'active': (bool,),  # noqa: E501
        }

    @cached_property
    def discriminator():
        return None


    attribute_map = {
        'logo': 'logo',  # noqa: E501
        'exchange': 'exchange',  # noqa: E501
        'exchange_symbol': 'exchangeSymbol',  # noqa: E501
        'type': 'type',  # noqa: E501
        'name': 'name',  # noqa: E501
        'symbol': 'symbol',  # noqa: E501
        'listdate': 'listdate',  # noqa: E501
        'cik': 'cik',  # noqa: E501
        'bloomberg': 'bloomberg',  # noqa: E501
        'figi': 'figi',  # noqa: E501
        'lei': 'lei',  # noqa: E501
        'sic': 'sic',  # noqa: E501
        'country': 'country',  # noqa: E501
        'industry': 'industry',  # noqa: E501
        'sector': 'sector',  # noqa: E501
        'marketcap': 'marketcap',  # noqa: E501
        'employees': 'employees',  # noqa: E501
        'phone': 'phone',  # noqa: E501
        'ceo': 'ceo',  # noqa: E501
        'url': 'url',  # noqa: E501
        'description': 'description',  # noqa: E501
        'hq_address': 'hq_address',  # noqa: E501
        'hq_state': 'hq_state',  # noqa: E501
        'hq_country': 'hq_country',  # noqa: E501
        'similar': 'similar',  # noqa: E501
        'tags': 'tags',  # noqa: E501
        'updated': 'updated',  # noqa: E501
        'active': 'active',  # noqa: E501
    }

    _composed_schemas = {}

    required_properties = set([
        '_data_store',
        '_check_type',
        '_spec_property_naming',
        '_path_to_item',
        '_configuration',
        '_visited_composed_classes',
    ])

    @convert_js_args_to_python_args
    def __init__(self, *args, **kwargs):  # noqa: E501
        """InlineResponse2001 - a model defined in OpenAPI

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            logo (str): The URL of the entity's logo.. [optional]  # noqa: E501
            exchange (str): The symbol's primary exchange.. [optional]  # noqa: E501
            exchange_symbol (str): The exchange code (id) of the symbol's primary exchange.. [optional]  # noqa: E501
            type (str): The type or class of the security.  (<a alt=\"Full List of Ticker Types\" href=\"https://polygon.io/docs/get_v2_reference_types_anchor\">Full List of Ticker Types</a>). [optional]  # noqa: E501
            name (str): The name of the company/entity.. [optional]  # noqa: E501
            symbol (str): The exchange symbol that this item is traded under.. [optional]  # noqa: E501
            listdate (date): The date that the symbol was listed on the exchange.. [optional]  # noqa: E501
            cik (str): The official CIK guid used for SEC database/filings.. [optional]  # noqa: E501
            bloomberg (str): The Bloomberg guid for the symbol.. [optional]  # noqa: E501
            figi (str): The OpenFigi project guid for the symbol. (<a rel=\"nofollow\" target=\"_blank\" href=\"https://openfigi.com/\">https://openfigi.com/</a>). [optional]  # noqa: E501
            lei (str): The Legal Entity Identifier (LEI) guid for the symbol. (<a rel=\"nofollow\" target=\"_blank\" href=\"https://en.wikipedia.org/wiki/Legal_Entity_Identifier\">https://en.wikipedia.org/wiki/Legal_Entity_Identifier</a>). [optional]  # noqa: E501
            sic (int): Standard Industrial Classification (SIC) id for the symbol. (<a rel=\"nofollow\" target=\"_blank\" href=\"https://en.wikipedia.org/wiki/Standard_Industrial_Classification\">https://en.wikipedia.org/wiki/Legal_Entity_Identifier</a>). [optional]  # noqa: E501
            country (str): The country in which the company is registered.. [optional]  # noqa: E501
            industry (str): The industry in which the company operates.. [optional]  # noqa: E501
            sector (str): The sector of the indsutry in which the symbol operates.. [optional]  # noqa: E501
            marketcap (int): The current market cap for the company.. [optional]  # noqa: E501
            employees (int): The approximate number of employees for the company.. [optional]  # noqa: E501
            phone (str): The phone number for the company. This is usually a corporate contact number.. [optional]  # noqa: E501
            ceo (str): The name of the company's current CEO.. [optional]  # noqa: E501
            url (str): The URL of the company's website. [optional]  # noqa: E501
            description (str): A description of the company and what they do/offer.. [optional]  # noqa: E501
            hq_address (str): The street address for the company's headquarters.. [optional]  # noqa: E501
            hq_state (str): The state in which the company's headquarters is located.. [optional]  # noqa: E501
            hq_country (str): The country in which the company's headquarters is located.. [optional]  # noqa: E501
            similar ([str]): A list of ticker symbols for similar companies.. [optional]  # noqa: E501
            tags ([str]): [optional]  # noqa: E501
            updated (date): The last time this company record was updated.. [optional]  # noqa: E501
            active (bool): Indicates if the security is actively listed.  If false, this means the company is no longer listed and cannot be traded.. [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', False)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        if args:
            raise ApiTypeError(
                "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                    args,
                    self.__class__.__name__,
                ),
                path_to_item=_path_to_item,
                valid_classes=(self.__class__,),
            )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
