"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from openapi_client.model_utils import (  # noqa: F401
    ApiTypeError,
    ModelComposed,
    ModelNormal,
    ModelSimple,
    cached_property,
    change_keys_js_to_python,
    convert_js_args_to_python_args,
    date,
    datetime,
    file_type,
    none_type,
    validate_get_composed_info,
)

def lazy_import():
    from openapi_client.model.vx_ticker_details_results_address import VXTickerDetailsResultsAddress
    globals()['VXTickerDetailsResultsAddress'] = VXTickerDetailsResultsAddress


class VXTickerDetailsResults(ModelNormal):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.

    Attributes:
      allowed_values (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          with a capitalized key describing the allowed value and an allowed
          value. These dicts store the allowed enum values.
      attribute_map (dict): The key is attribute name
          and the value is json key in definition.
      discriminator_value_class_map (dict): A dict to go from the discriminator
          variable value to the discriminator class name.
      validations (dict): The key is the tuple path to the attribute
          and the for var_name this is (var_name,). The value is a dict
          that stores validations for max_length, min_length, max_items,
          min_items, exclusive_maximum, inclusive_maximum, exclusive_minimum,
          inclusive_minimum, and regex.
      additional_properties_type (tuple): A tuple of classes accepted
          as additional properties values.
    """

    allowed_values = {
        ('market',): {
            'STOCKS': "stocks",
            'CRYPTO': "crypto",
            'FX': "fx",
        },
        ('locale',): {
            'US': "us",
            'GLOBAL': "global",
        },
    }

    validations = {
    }

    additional_properties_type = None

    _nullable = False

    @cached_property
    def openapi_types():
        """
        This must be a method because a model may have properties that are
        of type self, this must run after the class is loaded

        Returns
            openapi_types (dict): The key is attribute name
                and the value is attribute type.
        """
        lazy_import()
        return {
            'ticker': (str,),  # noqa: E501
            'name': (str,),  # noqa: E501
            'market': (str,),  # noqa: E501
            'locale': (str,),  # noqa: E501
            'primary_exchange': (str,),  # noqa: E501
            'type': (str,),  # noqa: E501
            'active': (bool,),  # noqa: E501
            'currency_name': (str,),  # noqa: E501
            'cik': (str,),  # noqa: E501
            'composite_figi': (str,),  # noqa: E501
            'share_class_figi': (str,),  # noqa: E501
            'last_updated_utc': (datetime,),  # noqa: E501
            'delisted_utc': (datetime,),  # noqa: E501
            'outstanding_shares': (float,),  # noqa: E501
            'market_cap': (float,),  # noqa: E501
            'phone_number': (str,),  # noqa: E501
            'address': (VXTickerDetailsResultsAddress,),  # noqa: E501
            'sic_code': (str,),  # noqa: E501
            'sic_description': (str,),  # noqa: E501
        }

    @cached_property
    def discriminator():
        return None


    attribute_map = {
        'ticker': 'ticker',  # noqa: E501
        'name': 'name',  # noqa: E501
        'market': 'market',  # noqa: E501
        'locale': 'locale',  # noqa: E501
        'primary_exchange': 'primary_exchange',  # noqa: E501
        'type': 'type',  # noqa: E501
        'active': 'active',  # noqa: E501
        'currency_name': 'currency_name',  # noqa: E501
        'cik': 'cik',  # noqa: E501
        'composite_figi': 'composite_figi',  # noqa: E501
        'share_class_figi': 'share_class_figi',  # noqa: E501
        'last_updated_utc': 'last_updated_utc',  # noqa: E501
        'delisted_utc': 'delisted_utc',  # noqa: E501
        'outstanding_shares': 'outstanding_shares',  # noqa: E501
        'market_cap': 'market_cap',  # noqa: E501
        'phone_number': 'phone_number',  # noqa: E501
        'address': 'address',  # noqa: E501
        'sic_code': 'sic_code',  # noqa: E501
        'sic_description': 'sic_description',  # noqa: E501
    }

    _composed_schemas = {}

    required_properties = set([
        '_data_store',
        '_check_type',
        '_spec_property_naming',
        '_path_to_item',
        '_configuration',
        '_visited_composed_classes',
    ])

    @convert_js_args_to_python_args
    def __init__(self, *args, **kwargs):  # noqa: E501
        """VXTickerDetailsResults - a model defined in OpenAPI

        Keyword Args:
            _check_type (bool): if True, values for parameters in openapi_types
                                will be type checked and a TypeError will be
                                raised if the wrong type is input.
                                Defaults to True
            _path_to_item (tuple/list): This is a list of keys or values to
                                drill down to the model in received_data
                                when deserializing a response
            _spec_property_naming (bool): True if the variable names in the input data
                                are serialized names, as specified in the OpenAPI document.
                                False if the variable names in the input data
                                are pythonic names, e.g. snake case (default)
            _configuration (Configuration): the instance to use when
                                deserializing a file_type parameter.
                                If passed, type conversion is attempted
                                If omitted no type conversion is done.
            _visited_composed_classes (tuple): This stores a tuple of
                                classes that we have traveled through so that
                                if we see that class again we will not use its
                                discriminator again.
                                When traveling through a discriminator, the
                                composed schema that is
                                is traveled through is added to this set.
                                For example if Animal has a discriminator
                                petType and we pass in "Dog", and the class Dog
                                allOf includes Animal, we move through Animal
                                once using the discriminator, and pick Dog.
                                Then in Dog, we will make an instance of the
                                Animal class but this time we won't travel
                                through its discriminator because we passed in
                                _visited_composed_classes = (Animal,)
            ticker (str): The exchange symbol that this item is traded under.. [optional]  # noqa: E501
            name (str): The name of the asset. For stocks/equities this will be the companies registered name. For crypto/fx this will be the name of the currency or coin pair. . [optional]  # noqa: E501
            market (str): The market type of the asset.. [optional]  # noqa: E501
            locale (str): The locale of the asset.. [optional]  # noqa: E501
            primary_exchange (str): The ISO code of the primary listing exchange for this asset.. [optional]  # noqa: E501
            type (str): The type of the asset. Find the types that we support via our [Ticker Types API](https://polygon.io/docs/get_v2_reference_types_anchor).. [optional]  # noqa: E501
            active (bool): Whether or not the asset is actively traded. False means the asset has been delisted.. [optional]  # noqa: E501
            currency_name (str): The name of the currency that this asset is traded with.. [optional]  # noqa: E501
            cik (str): The CIK number for this ticker. Find more information [here](https://en.wikipedia.org/wiki/Central_Index_Key).. [optional]  # noqa: E501
            composite_figi (str): The composite OpenFIGI number for this ticker. Find more information [here](https://www.openfigi.com/assets/content/Open_Symbology_Fields-2a61f8aa4d.pdf). [optional]  # noqa: E501
            share_class_figi (str): The share Class OpenFIGI number for this ticker. Find more information [here](https://www.openfigi.com/assets/content/Open_Symbology_Fields-2a61f8aa4d.pdf). [optional]  # noqa: E501
            last_updated_utc (datetime): The last time this asset record was updated.. [optional]  # noqa: E501
            delisted_utc (datetime): The last date that the asset was traded.. [optional]  # noqa: E501
            outstanding_shares (float): The recorded number of outstanding shares.. [optional]  # noqa: E501
            market_cap (float): The most recent close price of the ticker multiplied by outstanding shares.. [optional]  # noqa: E501
            phone_number (str): The phone number for the company behind this ticker.. [optional]  # noqa: E501
            address (VXTickerDetailsResultsAddress): [optional]  # noqa: E501
            sic_code (str): The standard industrial classification code for this ticker.  For a list of SIC Codes, see the SEC's <a rel=\"nofollow\" target=\"_blank\" href=\"https://www.sec.gov/info/edgar/siccodes.htm\">SIC Code List</a>. . [optional]  # noqa: E501
            sic_description (str): A description of this ticker's SIC code.. [optional]  # noqa: E501
        """

        _check_type = kwargs.pop('_check_type', True)
        _spec_property_naming = kwargs.pop('_spec_property_naming', False)
        _path_to_item = kwargs.pop('_path_to_item', ())
        _configuration = kwargs.pop('_configuration', None)
        _visited_composed_classes = kwargs.pop('_visited_composed_classes', ())

        if args:
            raise ApiTypeError(
                "Invalid positional arguments=%s passed to %s. Remove those invalid positional arguments." % (
                    args,
                    self.__class__.__name__,
                ),
                path_to_item=_path_to_item,
                valid_classes=(self.__class__,),
            )

        self._data_store = {}
        self._check_type = _check_type
        self._spec_property_naming = _spec_property_naming
        self._path_to_item = _path_to_item
        self._configuration = _configuration
        self._visited_composed_classes = _visited_composed_classes + (self.__class__,)

        for var_name, var_value in kwargs.items():
            if var_name not in self.attribute_map and \
                        self._configuration is not None and \
                        self._configuration.discard_unknown_keys and \
                        self.additional_properties_type is None:
                # discard variable.
                continue
            setattr(self, var_name, var_value)
