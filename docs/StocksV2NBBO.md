# StocksV2NBBO


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**t** | **str** | The exchange symbol that this item is traded under. | [optional] 
**t** | **int** | The nanosecond accuracy SIP Unix Timestamp. This is the timestamp of when the SIP received this message from the exchange which produced it. | [optional] 
**y** | **int** | The nanosecond accuracy Participant/Exchange Unix Timestamp. This is the timestamp of when the quote was actually generated at the exchange. | [optional] 
**f** | **int** | The nanosecond accuracy TRF(Trade Reporting Facility) Unix Timestamp. This is the timestamp of when the trade reporting facility received this message. | [optional] 
**q** | **int** | The sequence number represents the sequence in which message events happened. These are increasing and unique per ticker symbol, but will not always be sequential (e.g., 1, 2, 6, 9, 10, 11).  | [optional] 
**c** | **[int]** | A list of condition codes.  | [optional] 
**i** | **[int]** | The indicators. For more information, see our glossary of [Conditions and Indicators](https://polygon.io/glossary/us/stocks/conditions-indicators).  | [optional] 
**p** | **float** | The bid price. | [optional] 
**s** | **int** | The bid size. This represents the number of round lot orders at the given bid price. The normal round lot size is 100 shares. A bid size of 2 means there are 200 shares for purchase at the given bid price. | [optional] 
**x** | **object** |  | [optional] 
**p** | **float** | The ask price. | [optional] 
**s** | **int** | The ask size. This represents the number of round lot orders at the given ask price. The normal round lot size is 100 shares. An ask size of 2 means there are 200 shares available to purchase at the given ask price. | [optional] 
**x** | **object** |  | [optional] 
**z** | **int** | There are 3 tapes which define which exchange the ticker is listed on. These are integers in our objects which represent the letter of the alphabet. Eg: 1 &#x3D; A, 2 &#x3D; B, 3 &#x3D; C. * Tape A is NYSE listed securities * Tape B is NYSE ARCA / NYSE American * Tape C is NASDAQ  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


