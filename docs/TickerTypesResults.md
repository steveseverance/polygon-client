# TickerTypesResults


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**TickerTypesResultsTypes**](TickerTypesResultsTypes.md) |  | [optional] 
**index_types** | [**TickerTypesResultsIndexTypes**](TickerTypesResultsIndexTypes.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


