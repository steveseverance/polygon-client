# Markets


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[MarketsResults]**](MarketsResults.md) | A list of supported markets. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


