# InlineResponse2006


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | The ID of the exchange. | [optional] 
**type** | **str** | The type of exchange. - TRF &#x3D; Trade Reporting Facility - exchange &#x3D; Reporting exchange on the tape  | [optional] 
**market** | **str** | The market data type that this exchange contains. | [optional] 
**mic** | **str** | The Market Identification Code or MIC as defined in ISO 10383 (&lt;a rel&#x3D;\&quot;nofollow\&quot; target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;https://en.wikipedia.org/wiki/Market_Identifier_Code\&quot;&gt;https://en.wikipedia.org/wiki/Market_Identifier_Code&lt;/a&gt;). | [optional] 
**name** | **str** | The name of the exchange. | [optional] 
**tape** | **str** | The tape id of the exchange. | [optional] 
**code** | **str** | A unique identifier for the exchange internal to Polygon.io.  This is not an industry code or ISO standard. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


