# DividendResults


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticker** | **str** | The exchange symbol that this item is traded under. | [optional] 
**ex_date** | **date** | The execution date of the dividend. | [optional] 
**payment_date** | **date** | The payment date of the dividend. | [optional] 
**record_date** | **date** | The date of record for the dividend.  See &lt;a rel&#x3D;\&quot;nofollow\&quot; target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;https://www.investor.gov/introduction-investing/investing-basics/glossary/ex-dividend-dates-when-are-you-entitled-stock-and#:~:text&#x3D;The%20ex%2Ddividend%20date%20for,the%20seller%20gets%20the%20dividend.\&quot; alt&#x3D;\&quot;Investor.gov&#39;s explanation of dividend dates\&quot;&gt;Investor.gov&lt;/a&gt; for an explanation on dividend dates and their meaning.  | [optional] 
**amount** | **float** | The amount of the dividend. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


