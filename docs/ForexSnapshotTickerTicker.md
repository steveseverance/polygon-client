# ForexSnapshotTickerTicker


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | [**ForexSnapshotTickerTickerDay**](ForexSnapshotTickerTickerDay.md) |  | [optional] 
**last_quote** | [**ForexSnapshotTickerTickerLastQuote**](ForexSnapshotTickerTickerLastQuote.md) |  | [optional] 
**min** | [**ForexSnapshotTickerTickerMin**](ForexSnapshotTickerTickerMin.md) |  | [optional] 
**prev_day** | [**StocksSnapshotTickersDay**](StocksSnapshotTickersDay.md) |  | [optional] 
**ticker** | **str** | The exchange symbol that this item is traded under. | [optional] 
**todays_change** | **float** | The value of the change the from previous day. | [optional] 
**todays_change_perc** | **float** | The percentage change since the previous day. | [optional] 
**updated** | **int** | The last updated timestamp. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


