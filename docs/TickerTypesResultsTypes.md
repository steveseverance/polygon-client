# TickerTypesResultsTypes


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cs** | **str** | Common Stock | [optional] 
**adr** | **str** | American Depository Receipt | [optional] 
**nvdr** | **str** | Non-Voting Depository Receipt | [optional] 
**gdr** | **str** | Global Depositary Receipt | [optional] 
**sdr** | **str** | Special Drawing Right | [optional] 
**cef** | **str** | Closed-End Fund | [optional] 
**etp** | **str** | Exchange Traded Product/Fund | [optional] 
**reit** | **str** | Real Estate Investment Trust | [optional] 
**mlp** | **str** | Master Limited Partnership | [optional] 
**wrt** | **str** | Equity WRT | [optional] 
**pub** | **str** | Public | [optional] 
**nyrs** | **str** | New York Registry Shares | [optional] 
**unit** | **str** | Unit | [optional] 
**right** | **str** | Right | [optional] 
**trak** | **str** | Tracking stock or targeted stock | [optional] 
**ltdp** | **str** | Limited Partnership | [optional] 
**rylt** | **str** | Royalty Trust | [optional] 
**mf** | **str** | Mutual Fund | [optional] 
**pfd** | **str** | Preferred Stock | [optional] 
**fdr** | **str** | Foreign Ordinary Shares | [optional] 
**ost** | **str** | Other Security Type | [optional] 
**fund** | **str** | Fund | [optional] 
**sp** | **str** | Structured Product | [optional] 
**si** | **str** | Secondary Issue | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


