# openapi_client.CryptoApi

All URIs are relative to *https://api.polygon.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get**](CryptoApi.md#v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get) | **GET** /v2/aggs/ticker/{cryptoTicker}/range/{multiplier}/{timespan}/{from}/{to} | Aggregates (Bars)


# **v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get**
> object v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get(crypto_ticker, multiplier, timespan, _from, to)

Aggregates (Bars)

Get aggregate bars for a cryptocurrency pair over a given date range in custom time window sizes. <br /> <br /> For example, if timespan = ‘minute’ and multiplier = ‘5’ then 5-minute bars will be returned. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = crypto_api.CryptoApi(api_client)
    crypto_ticker = "X:BTCUSD" # str | The ticker symbol of the currency pair.
    multiplier = 1 # int | The size of the timespan multiplier.
    timespan = "day" # str | The size of the time window.
    _from = "2020-10-14" # str | The start of the aggregate time window.
    to = "2020-10-14" # str | The end of the aggregate time window.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)
    sort = "asc" # str | Sort the results by timestamp. `asc` will return results in ascending order (oldest at the top), `desc` will return results in descending order (newest at the top).  (optional)
    limit = 120 # int | Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  <a href=\"https://polygon.io/blog/aggs-api-updates/\" target=\"_blank\" alt=\"Aggregate Data API Improvements\">Aggregate Data API Improvements</a>.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get(crypto_ticker, multiplier, timespan, _from, to)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CryptoApi->v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get(crypto_ticker, multiplier, timespan, _from, to, unadjusted=unadjusted, sort=sort, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CryptoApi->v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **crypto_ticker** | **str**| The ticker symbol of the currency pair. |
 **multiplier** | **int**| The size of the timespan multiplier. |
 **timespan** | **str**| The size of the time window. |
 **_from** | **str**| The start of the aggregate time window. |
 **to** | **str**| The end of the aggregate time window. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]
 **sort** | **str**| Sort the results by timestamp. &#x60;asc&#x60; will return results in ascending order (oldest at the top), &#x60;desc&#x60; will return results in descending order (newest at the top).  | [optional]
 **limit** | **int**| Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  &lt;a href&#x3D;\&quot;https://polygon.io/blog/aggs-api-updates/\&quot; target&#x3D;\&quot;_blank\&quot; alt&#x3D;\&quot;Aggregate Data API Improvements\&quot;&gt;Aggregate Data API Improvements&lt;/a&gt;.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Cryptocurrency Aggregates. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

