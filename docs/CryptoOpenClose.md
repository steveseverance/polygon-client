# CryptoOpenClose


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**symbol** | **str** | The symbol pair that was evaluated from the request. | [optional] 
**is_utc** | **bool** | Whether or not the timestamps are in UTC timezone. | [optional] 
**day** | **date** | The date requested. | [optional] 
**open** | **float** | The open price for the symbol in the given time period. | [optional] 
**close** | **float** | The close price for the symbol in the given time period. | [optional] 
**open_trades** | [**[InlineResponse20010OpenTrades]**](InlineResponse20010OpenTrades.md) |  | [optional] 
**closing_trades** | [**[InlineResponse20010OpenTrades]**](InlineResponse20010OpenTrades.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


