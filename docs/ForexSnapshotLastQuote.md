# ForexSnapshotLastQuote


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**a** | **float** | The ask price. | [optional] 
**b** | **float** | The bid price. | [optional] 
**t** | **int** | The Unix Msec timestamp for the start of the aggregate window. | [optional] 
**x** | **int** | The exchange ID on which this quote happened. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


