# openapi_client.StocksEquitiesApi

All URIs are relative to *https://api.polygon.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1_last_quote_stocks_stocks_ticker_get**](StocksEquitiesApi.md#v1_last_quote_stocks_stocks_ticker_get) | **GET** /v1/last_quote/stocks/{stocksTicker} | Last Quote for a Symbol
[**v1_last_stocks_stocks_ticker_get**](StocksEquitiesApi.md#v1_last_stocks_stocks_ticker_get) | **GET** /v1/last/stocks/{stocksTicker} | Last Trade for a Symbol
[**v1_meta_conditions_ticktype_get**](StocksEquitiesApi.md#v1_meta_conditions_ticktype_get) | **GET** /v1/meta/conditions/{ticktype} | Condition Mappings
[**v1_meta_exchanges_get**](StocksEquitiesApi.md#v1_meta_exchanges_get) | **GET** /v1/meta/exchanges | Stock Exchanges
[**v1_open_close_stocks_ticker_date_get**](StocksEquitiesApi.md#v1_open_close_stocks_ticker_date_get) | **GET** /v1/open-close/{stocksTicker}/{date} | Daily Open/Close
[**v2_aggs_grouped_locale_us_market_stocks_date_get**](StocksEquitiesApi.md#v2_aggs_grouped_locale_us_market_stocks_date_get) | **GET** /v2/aggs/grouped/locale/us/market/stocks/{date} | Grouped Daily (Bars)
[**v2_aggs_ticker_stocks_ticker_prev_get**](StocksEquitiesApi.md#v2_aggs_ticker_stocks_ticker_prev_get) | **GET** /v2/aggs/ticker/{stocksTicker}/prev | Previous Close
[**v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get**](StocksEquitiesApi.md#v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get) | **GET** /v2/aggs/ticker/{stocksTicker}/range/{multiplier}/{timespan}/{from}/{to} | Aggregates (Bars)
[**v2_last_nbbo_stocks_ticker_get**](StocksEquitiesApi.md#v2_last_nbbo_stocks_ticker_get) | **GET** /v2/last/nbbo/{stocksTicker} | Last NBBO for a Symbol
[**v2_last_trade_stocks_ticker_get**](StocksEquitiesApi.md#v2_last_trade_stocks_ticker_get) | **GET** /v2/last/trade/{stocksTicker} | Last Trade for a Symbol
[**v2_snapshot_locale_us_markets_stocks_direction_get**](StocksEquitiesApi.md#v2_snapshot_locale_us_markets_stocks_direction_get) | **GET** /v2/snapshot/locale/us/markets/stocks/{direction} | Snapshot - Gainers/Losers
[**v2_snapshot_locale_us_markets_stocks_tickers_get**](StocksEquitiesApi.md#v2_snapshot_locale_us_markets_stocks_tickers_get) | **GET** /v2/snapshot/locale/us/markets/stocks/tickers | Snapshot - All Tickers
[**v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get**](StocksEquitiesApi.md#v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get) | **GET** /v2/snapshot/locale/us/markets/stocks/tickers/{stocksTicker} | Snapshot - Ticker
[**v2_ticks_stocks_nbbo_ticker_date_get**](StocksEquitiesApi.md#v2_ticks_stocks_nbbo_ticker_date_get) | **GET** /v2/ticks/stocks/nbbo/{ticker}/{date} | Quotes (NBBO)
[**v2_ticks_stocks_trades_ticker_date_get**](StocksEquitiesApi.md#v2_ticks_stocks_trades_ticker_date_get) | **GET** /v2/ticks/stocks/trades/{ticker}/{date} | Trades


# **v1_last_quote_stocks_stocks_ticker_get**
> object v1_last_quote_stocks_stocks_ticker_get(stocks_ticker)

Last Quote for a Symbol

Get the most recent quote tick for a given stock. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Last Quote for a Symbol
        api_response = api_instance.v1_last_quote_stocks_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v1_last_quote_stocks_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last quote tick for this stock. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_last_stocks_stocks_ticker_get**
> object v1_last_stocks_stocks_ticker_get(stocks_ticker)

Last Trade for a Symbol

Get the most recent trade for a given stock. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Last Trade for a Symbol
        api_response = api_instance.v1_last_stocks_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v1_last_stocks_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last trade for this stock. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_meta_conditions_ticktype_get**
> InlineResponse2008 v1_meta_conditions_ticktype_get(ticktype)

Condition Mappings

Get a unified numerical mapping for conditions on trades and quotes.  Each feed/exchange uses its own set of codes to identify conditions, so the same condition may have a different code depending on the originator of the data. Polygon.io defines its own mapping to allow for uniformly identifying a condition across feeds/exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from openapi_client.model.inline_response2008 import InlineResponse2008
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    ticktype = "trades" # str | The type of ticks to return mappings for. 

    # example passing only required values which don't have defaults set
    try:
        # Condition Mappings
        api_response = api_instance.v1_meta_conditions_ticktype_get(ticktype)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v1_meta_conditions_ticktype_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticktype** | **str**| The type of ticks to return mappings for.  |

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of condition mappings. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_meta_exchanges_get**
> [InlineResponse2006] v1_meta_exchanges_get()

Stock Exchanges

Get a list of stock exchanges which are supported by Polygon.io. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from openapi_client.model.inline_response2006 import InlineResponse2006
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Stock Exchanges
        api_response = api_instance.v1_meta_exchanges_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v1_meta_exchanges_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[InlineResponse2006]**](InlineResponse2006.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of supported stock exchanges. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_open_close_stocks_ticker_date_get**
> InlineResponse2007 v1_open_close_stocks_ticker_date_get(stocks_ticker, date)

Daily Open/Close

Get the open, close and afterhours prices of a stock symbol on a certain date. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from openapi_client.model.inline_response2007 import InlineResponse2007
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.
    date = dateutil_parser('2020-10-14').date() # date | The date of the requested open/close in the format YYYY-MM-DD.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Daily Open/Close
        api_response = api_instance.v1_open_close_stocks_ticker_date_get(stocks_ticker, date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v1_open_close_stocks_ticker_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Daily Open/Close
        api_response = api_instance.v1_open_close_stocks_ticker_date_get(stocks_ticker, date, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v1_open_close_stocks_ticker_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |
 **date** | **date**| The date of the requested open/close in the format YYYY-MM-DD. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The open/close of this stock symbol. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_grouped_locale_us_market_stocks_date_get**
> object v2_aggs_grouped_locale_us_market_stocks_date_get(date)

Grouped Daily (Bars)

Get the daily open, high, low, and close (OHLC) for the entire stocks/equities markets. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    date = "2020-10-14" # str | The beginning date for the aggregate window.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Grouped Daily (Bars)
        api_response = api_instance.v2_aggs_grouped_locale_us_market_stocks_date_get(date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_aggs_grouped_locale_us_market_stocks_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Grouped Daily (Bars)
        api_response = api_instance.v2_aggs_grouped_locale_us_market_stocks_date_get(date, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_aggs_grouped_locale_us_market_stocks_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **str**| The beginning date for the aggregate window. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Previous day OHLC for ticker |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_ticker_stocks_ticker_prev_get**
> object v2_aggs_ticker_stocks_ticker_prev_get(stocks_ticker)

Previous Close

Get the previous day's open, high, low, and close (OHLC) for the specified stock ticker. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Previous Close
        api_response = api_instance.v2_aggs_ticker_stocks_ticker_prev_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_aggs_ticker_stocks_ticker_prev_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Previous Close
        api_response = api_instance.v2_aggs_ticker_stocks_ticker_prev_get(stocks_ticker, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_aggs_ticker_stocks_ticker_prev_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The previous day OHLC for the ticker. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get**
> object v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get(stocks_ticker, multiplier, timespan, _from, to)

Aggregates (Bars)

Get aggregate bars for a stock over a given date range in custom time window sizes. <br /> <br /> For example, if timespan = ‘minute’ and multiplier = ‘5’ then 5-minute bars will be returned. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.
    multiplier = 1 # int | The size of the timespan multiplier.
    timespan = "day" # str | The size of the time window.
    _from = "2020-10-14" # str | The start of the aggregate time window.
    to = "2020-10-14" # str | The end of the aggregate time window.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)
    sort = "asc" # str | Sort the results by timestamp. `asc` will return results in ascending order (oldest at the top), `desc` will return results in descending order (newest at the top).  (optional)
    limit = 120 # int | Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  <a href=\"https://polygon.io/blog/aggs-api-updates/\" target=\"_blank\" alt=\"Aggregate Data API Improvements\">Aggregate Data API Improvements</a>.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get(stocks_ticker, multiplier, timespan, _from, to)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get(stocks_ticker, multiplier, timespan, _from, to, unadjusted=unadjusted, sort=sort, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |
 **multiplier** | **int**| The size of the timespan multiplier. |
 **timespan** | **str**| The size of the time window. |
 **_from** | **str**| The start of the aggregate time window. |
 **to** | **str**| The end of the aggregate time window. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]
 **sort** | **str**| Sort the results by timestamp. &#x60;asc&#x60; will return results in ascending order (oldest at the top), &#x60;desc&#x60; will return results in descending order (newest at the top).  | [optional]
 **limit** | **int**| Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  &lt;a href&#x3D;\&quot;https://polygon.io/blog/aggs-api-updates/\&quot; target&#x3D;\&quot;_blank\&quot; alt&#x3D;\&quot;Aggregate Data API Improvements\&quot;&gt;Aggregate Data API Improvements&lt;/a&gt;.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Stock Aggregates. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_last_nbbo_stocks_ticker_get**
> object v2_last_nbbo_stocks_ticker_get(stocks_ticker)

Last NBBO for a Symbol

Get the most recent NBBO tick for a given stock. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Last NBBO for a Symbol
        api_response = api_instance.v2_last_nbbo_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_last_nbbo_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last NBBO tick for this stock. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_last_trade_stocks_ticker_get**
> object v2_last_trade_stocks_ticker_get(stocks_ticker)

Last Trade for a Symbol

Get the most recent trade for a given stock. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Last Trade for a Symbol
        api_response = api_instance.v2_last_trade_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_last_trade_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last trade for this stock. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_us_markets_stocks_direction_get**
> object v2_snapshot_locale_us_markets_stocks_direction_get(direction)

Snapshot - Gainers/Losers

Get the current top 20 gainers or losers of the day in stocks/equities markets. <br /> <br /> Top gainers are those tickers whose price has increased by the highest percentage since the previous day's close. Top losers are those tickers whose price has decreased by the highest percentage since the previous day's close. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    direction = "gainers" # str | The direction of the snapshot results to return. 

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Gainers/Losers
        api_response = api_instance.v2_snapshot_locale_us_markets_stocks_direction_get(direction)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_snapshot_locale_us_markets_stocks_direction_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **direction** | **str**| The direction of the snapshot results to return.  |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get the current tickers of the day |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_us_markets_stocks_tickers_get**
> object v2_snapshot_locale_us_markets_stocks_tickers_get()

Snapshot - All Tickers

Get the current minute, day, and previous day’s aggregate, as well as the last trade and quote for all traded stock symbols. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. This can happen as early as 4am EST. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    tickers = [
        "tickers_example",
    ] # [str] | A comma separated list of tickers to get snapshots for. (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Snapshot - All Tickers
        api_response = api_instance.v2_snapshot_locale_us_markets_stocks_tickers_get(tickers=tickers)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_snapshot_locale_us_markets_stocks_tickers_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tickers** | **[str]**| A comma separated list of tickers to get snapshots for. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current state for all tickers |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get**
> object v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get(stocks_ticker)

Snapshot - Ticker

Get the current minute, day, and previous day’s aggregate, as well as the last trade and quote for a single traded stock ticker. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. This can happen as early as 4am EST. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Ticker
        api_response = api_instance.v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current state for a ticker |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_ticks_stocks_nbbo_ticker_date_get**
> object v2_ticks_stocks_nbbo_ticker_date_get(ticker, date)

Quotes (NBBO)

Get NBBO quotes for a given ticker symbol on a specified date. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    ticker = "AAPL" # str | The ticker symbol we want quotes for.
    date = dateutil_parser('2020-10-14').date() # date | The date/day of the quotes to retrieve in the format YYYY-MM-DD.
    timestamp = 1 # int | The timestamp offset, used for pagination. This is the offset at which to start the results. Using the `timestamp` of the last result as the offset will give you the next page of results.  (optional)
    timestamp_limit = 1 # int | The maximum timestamp allowed in the results.  (optional)
    reverse = True # bool | Reverse the order of the results.  (optional)
    limit = 10 # int | Limit the size of the response, max 50000 and default 5000. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Quotes (NBBO)
        api_response = api_instance.v2_ticks_stocks_nbbo_ticker_date_get(ticker, date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_ticks_stocks_nbbo_ticker_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Quotes (NBBO)
        api_response = api_instance.v2_ticks_stocks_nbbo_ticker_date_get(ticker, date, timestamp=timestamp, timestamp_limit=timestamp_limit, reverse=reverse, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_ticks_stocks_nbbo_ticker_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| The ticker symbol we want quotes for. |
 **date** | **date**| The date/day of the quotes to retrieve in the format YYYY-MM-DD. |
 **timestamp** | **int**| The timestamp offset, used for pagination. This is the offset at which to start the results. Using the &#x60;timestamp&#x60; of the last result as the offset will give you the next page of results.  | [optional]
 **timestamp_limit** | **int**| The maximum timestamp allowed in the results.  | [optional]
 **reverse** | **bool**| Reverse the order of the results.  | [optional]
 **limit** | **int**| Limit the size of the response, max 50000 and default 5000. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of quotes. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_ticks_stocks_trades_ticker_date_get**
> object v2_ticks_stocks_trades_ticker_date_get(ticker, date)

Trades

Get trades for a given ticker symbol on a specified date. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import stocks___equities_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = stocks___equities_api.StocksEquitiesApi(api_client)
    ticker = "AAPL" # str | The ticker symbol we want trades for.
    date = dateutil_parser('2020-10-14').date() # date | The date/day of the trades to retrieve in the format YYYY-MM-DD.
    timestamp = 1 # int | The timestamp offset, used for pagination. This is the offset at which to start the results. Using the `timestamp` of the last result as the offset will give you the next page of results.  (optional)
    timestamp_limit = 1 # int | The maximum timestamp allowed in the results.  (optional)
    reverse = True # bool | Reverse the order of the results.  (optional)
    limit = 10 # int | Limit the size of the response, max 50000 and default 5000. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Trades
        api_response = api_instance.v2_ticks_stocks_trades_ticker_date_get(ticker, date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_ticks_stocks_trades_ticker_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Trades
        api_response = api_instance.v2_ticks_stocks_trades_ticker_date_get(ticker, date, timestamp=timestamp, timestamp_limit=timestamp_limit, reverse=reverse, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling StocksEquitiesApi->v2_ticks_stocks_trades_ticker_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| The ticker symbol we want trades for. |
 **date** | **date**| The date/day of the trades to retrieve in the format YYYY-MM-DD. |
 **timestamp** | **int**| The timestamp offset, used for pagination. This is the offset at which to start the results. Using the &#x60;timestamp&#x60; of the last result as the offset will give you the next page of results.  | [optional]
 **timestamp_limit** | **int**| The maximum timestamp allowed in the results.  | [optional]
 **reverse** | **bool**| Reverse the order of the results.  | [optional]
 **limit** | **int**| Limit the size of the response, max 50000 and default 5000. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of trades. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

