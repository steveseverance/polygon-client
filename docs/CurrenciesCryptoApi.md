# openapi_client.CurrenciesCryptoApi

All URIs are relative to *https://api.polygon.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1_historic_crypto_from_to_date_get**](CurrenciesCryptoApi.md#v1_historic_crypto_from_to_date_get) | **GET** /v1/historic/crypto/{from}/{to}/{date} | Historic Crypto Trades
[**v1_last_crypto_from_to_get**](CurrenciesCryptoApi.md#v1_last_crypto_from_to_get) | **GET** /v1/last/crypto/{from}/{to} | Last Trade for a Crypto Pair
[**v1_meta_crypto_exchanges_get**](CurrenciesCryptoApi.md#v1_meta_crypto_exchanges_get) | **GET** /v1/meta/crypto-exchanges | Crypto Exchanges
[**v1_open_close_crypto_from_to_date_get**](CurrenciesCryptoApi.md#v1_open_close_crypto_from_to_date_get) | **GET** /v1/open-close/crypto/{from}/{to}/{date} | Daily Open/Close
[**v2_aggs_grouped_locale_global_market_crypto_date_get**](CurrenciesCryptoApi.md#v2_aggs_grouped_locale_global_market_crypto_date_get) | **GET** /v2/aggs/grouped/locale/global/market/crypto/{date} | Grouped Daily (Bars)
[**v2_aggs_grouped_locale_global_market_fx_date_get**](CurrenciesCryptoApi.md#v2_aggs_grouped_locale_global_market_fx_date_get) | **GET** /v2/aggs/grouped/locale/global/market/fx/{date} | Grouped Daily (Bars)
[**v2_aggs_ticker_crypto_ticker_prev_get**](CurrenciesCryptoApi.md#v2_aggs_ticker_crypto_ticker_prev_get) | **GET** /v2/aggs/ticker/{cryptoTicker}/prev | Previous Close
[**v2_snapshot_locale_global_markets_crypto_direction_get**](CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_direction_get) | **GET** /v2/snapshot/locale/global/markets/crypto/{direction} | Snapshot - Gainers/Losers
[**v2_snapshot_locale_global_markets_crypto_tickers_get**](CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_tickers_get) | **GET** /v2/snapshot/locale/global/markets/crypto/tickers | Snapshot - All Tickers
[**v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get**](CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get) | **GET** /v2/snapshot/locale/global/markets/crypto/tickers/{ticker}/book | Snapshot - Ticker Full Book (L2)
[**v2_snapshot_locale_global_markets_crypto_tickers_ticker_get**](CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_tickers_ticker_get) | **GET** /v2/snapshot/locale/global/markets/crypto/tickers/{ticker} | Snapshot - Ticker


# **v1_historic_crypto_from_to_date_get**
> object v1_historic_crypto_from_to_date_get(_from, to, date)

Historic Crypto Trades

Get historic trade ticks for a cryptocurrency pair. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    _from = "BTC" # str | The \"from\" symbol of the crypto pair.
    to = "USD" # str | The \"to\" symbol of the crypto pair.
    date = dateutil_parser('2020-10-14').date() # date | The date/day of the historic ticks to retrieve.
    offset = 1 # int | The timestamp offset, used for pagination. This is the offset at which to start the results. Using the `timestamp` of the last result as the offset will give you the next page of results.  (optional)
    limit = 100 # int | Limit the size of the response, max 10000. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Historic Crypto Trades
        api_response = api_instance.v1_historic_crypto_from_to_date_get(_from, to, date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v1_historic_crypto_from_to_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Historic Crypto Trades
        api_response = api_instance.v1_historic_crypto_from_to_date_get(_from, to, date, offset=offset, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v1_historic_crypto_from_to_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| The \&quot;from\&quot; symbol of the crypto pair. |
 **to** | **str**| The \&quot;to\&quot; symbol of the crypto pair. |
 **date** | **date**| The date/day of the historic ticks to retrieve. |
 **offset** | **int**| The timestamp offset, used for pagination. This is the offset at which to start the results. Using the &#x60;timestamp&#x60; of the last result as the offset will give you the next page of results.  | [optional]
 **limit** | **int**| Limit the size of the response, max 10000. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of crypto trade ticks. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_last_crypto_from_to_get**
> object v1_last_crypto_from_to_get(_from, to)

Last Trade for a Crypto Pair

Get the last trade tick for a cryptocurrency pair. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    _from = "BTC" # str | The \"from\" symbol of the pair.
    to = "USD" # str | The \"to\" symbol of the pair.

    # example passing only required values which don't have defaults set
    try:
        # Last Trade for a Crypto Pair
        api_response = api_instance.v1_last_crypto_from_to_get(_from, to)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v1_last_crypto_from_to_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| The \&quot;from\&quot; symbol of the pair. |
 **to** | **str**| The \&quot;to\&quot; symbol of the pair. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last tick for this currency pair. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_meta_crypto_exchanges_get**
> [InlineResponse2009] v1_meta_crypto_exchanges_get()

Crypto Exchanges

Get a list of cryptocurrency exchanges which are supported by Polygon.io. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from openapi_client.model.inline_response2009 import InlineResponse2009
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Crypto Exchanges
        api_response = api_instance.v1_meta_crypto_exchanges_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v1_meta_crypto_exchanges_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[InlineResponse2009]**](InlineResponse2009.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of crypto currency exchanges. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_open_close_crypto_from_to_date_get**
> InlineResponse20010 v1_open_close_crypto_from_to_date_get(_from, to, date)

Daily Open/Close

Get the open, close prices of a cryptocurrency symbol on a certain day. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from openapi_client.model.inline_response20010 import InlineResponse20010
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    _from = "BTC" # str | The \"from\" symbol of the pair.
    to = "USD" # str | The \"to\" symbol of the pair.
    date = dateutil_parser('2020-10-14').date() # date | The date of the requested open/close in the format YYYY-MM-DD.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Daily Open/Close
        api_response = api_instance.v1_open_close_crypto_from_to_date_get(_from, to, date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v1_open_close_crypto_from_to_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Daily Open/Close
        api_response = api_instance.v1_open_close_crypto_from_to_date_get(_from, to, date, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v1_open_close_crypto_from_to_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| The \&quot;from\&quot; symbol of the pair. |
 **to** | **str**| The \&quot;to\&quot; symbol of the pair. |
 **date** | **date**| The date of the requested open/close in the format YYYY-MM-DD. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The open/close of this symbol. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_grouped_locale_global_market_crypto_date_get**
> object v2_aggs_grouped_locale_global_market_crypto_date_get(date)

Grouped Daily (Bars)

Get the daily open, high, low, and close (OHLC) for the entire cryptocurrency markets. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    date = "2020-10-14" # str | The beginning date for the aggregate window.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Grouped Daily (Bars)
        api_response = api_instance.v2_aggs_grouped_locale_global_market_crypto_date_get(date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_aggs_grouped_locale_global_market_crypto_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Grouped Daily (Bars)
        api_response = api_instance.v2_aggs_grouped_locale_global_market_crypto_date_get(date, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_aggs_grouped_locale_global_market_crypto_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **str**| The beginning date for the aggregate window. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The previous day OHLC for the ticker. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_grouped_locale_global_market_fx_date_get**
> object v2_aggs_grouped_locale_global_market_fx_date_get(date)

Grouped Daily (Bars)

Get the daily open, high, low, and close (OHLC) for the entire forex markets. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    date = "2020-10-14" # str | The beginning date for the aggregate window.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Grouped Daily (Bars)
        api_response = api_instance.v2_aggs_grouped_locale_global_market_fx_date_get(date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_aggs_grouped_locale_global_market_fx_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Grouped Daily (Bars)
        api_response = api_instance.v2_aggs_grouped_locale_global_market_fx_date_get(date, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_aggs_grouped_locale_global_market_fx_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **str**| The beginning date for the aggregate window. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Previous day OHLC for ticker |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_ticker_crypto_ticker_prev_get**
> object v2_aggs_ticker_crypto_ticker_prev_get(crypto_ticker)

Previous Close

Get the previous day's open, high, low, and close (OHLC) for the specified cryptocurrency pair. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    crypto_ticker = "X:BTCUSD" # str | The ticker symbol of the currency pair.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Previous Close
        api_response = api_instance.v2_aggs_ticker_crypto_ticker_prev_get(crypto_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_aggs_ticker_crypto_ticker_prev_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Previous Close
        api_response = api_instance.v2_aggs_ticker_crypto_ticker_prev_get(crypto_ticker, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_aggs_ticker_crypto_ticker_prev_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **crypto_ticker** | **str**| The ticker symbol of the currency pair. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The previous day OHLC for a ticker. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_crypto_direction_get**
> object v2_snapshot_locale_global_markets_crypto_direction_get(direction)

Snapshot - Gainers/Losers

Get the current top 20 gainers or losers of the day in cryptocurrency markets. <br /> <br /> Top gainers are those tickers whose price has increased by the highest percentage since the previous day's close. Top losers are those tickers whose price has decreased by the highest percentage since the previous day's close. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    direction = "gainers" # str | The direction of the snapshot results to return. 

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Gainers/Losers
        api_response = api_instance.v2_snapshot_locale_global_markets_crypto_direction_get(direction)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_snapshot_locale_global_markets_crypto_direction_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **direction** | **str**| The direction of the snapshot results to return.  |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get the current gainers / losers of the day |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_crypto_tickers_get**
> object v2_snapshot_locale_global_markets_crypto_tickers_get()

Snapshot - All Tickers

Get the current minute, day, and previous day’s aggregate, as well as the last trade and quote for all traded cryptocurrency symbols. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. This can happen as early as 4am EST. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    tickers = [
        "tickers_example",
    ] # [str] | A comma separated list of tickers to get snapshots for. (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Snapshot - All Tickers
        api_response = api_instance.v2_snapshot_locale_global_markets_crypto_tickers_get(tickers=tickers)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_snapshot_locale_global_markets_crypto_tickers_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tickers** | **[str]**| A comma separated list of tickers to get snapshots for. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current state for all tickers |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get**
> object v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get(ticker)

Snapshot - Ticker Full Book (L2)

Get the current level 2 book of a single ticker. This is the combined book from all of the exchanges. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    ticker = "X:BTCUSD" # str | The cryptocurrency ticker.

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Ticker Full Book (L2)
        api_response = api_instance.v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get(ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| The cryptocurrency ticker. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current level 2 book for a ticker |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_crypto_tickers_ticker_get**
> object v2_snapshot_locale_global_markets_crypto_tickers_ticker_get(ticker)

Snapshot - Ticker

Get the current minute, day, and previous day’s aggregate, as well as the last trade and quote for a single traded cryptocurrency symbol. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___crypto_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___crypto_api.CurrenciesCryptoApi(api_client)
    ticker = "X:BTCUSD" # str | Ticker of the snapshot

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Ticker
        api_response = api_instance.v2_snapshot_locale_global_markets_crypto_tickers_ticker_get(ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesCryptoApi->v2_snapshot_locale_global_markets_crypto_tickers_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| Ticker of the snapshot |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current state for a ticker |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

