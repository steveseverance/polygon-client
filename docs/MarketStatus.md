# MarketStatus


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**market** | **str** | The status of the market as a whole. | [optional] 
**server_time** | **datetime** | The current time of the server. | [optional] 
**exchanges** | [**InlineResponse2005Exchanges**](InlineResponse2005Exchanges.md) |  | [optional] 
**currencies** | [**InlineResponse2005Currencies**](InlineResponse2005Currencies.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


