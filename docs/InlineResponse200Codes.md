# InlineResponse200Codes


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cik** | **str** | The CIK number for this ticker. | [optional] 
**figi** | **str** | The OpenFIGI number for this ticker. | [optional] 
**cfigi** | **str** | The composite OpenFIGI number for this ticker. | [optional] 
**scfigi** | **str** | The shared Class OpenFIGI number for this ticker. | [optional] 
**figiuid** | **str** | The unique OpenFIGI ID number for this ticker. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


