# Map

A mapping of the keys returned in the results to their descriptive name and data types.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | [**MapKey**](MapKey.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


