# InlineResponse2005Exchanges


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nyse** | **str** | The status of the NYSE market. | [optional] 
**nasdaq** | **str** | The status of the Nasdaq market. | [optional] 
**otc** | **str** | The status of the OTC market. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


