# CryptoSnapshotTickerFullBook


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticker** | **str** | The exchange symbol that this item is traded under. | [optional] 
**bids** | [**[CryptoSnapshotTickerFullBookBids]**](CryptoSnapshotTickerFullBookBids.md) |  | [optional] 
**asks** | [**[CryptoSnapshotTickerFullBookBids]**](CryptoSnapshotTickerFullBookBids.md) |  | [optional] 
**bid_count** | **float** | The combined total number of bids in the book. | [optional] 
**ask_count** | **float** | The combined total number of asks in the book. | [optional] 
**spread** | **float** | The difference between the best bid and the best ask price accross exchanges. | [optional] 
**updated** | **int** | The last updated timestamp. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


