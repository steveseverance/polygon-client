# StocksV2NBBOAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c** | **[int]** | A list of condition codes.  | [optional] 
**i** | **[int]** | The indicators. For more information, see our glossary of [Conditions and Indicators](https://polygon.io/glossary/us/stocks/conditions-indicators).  | [optional] 
**p** | **float** | The bid price. | [optional] 
**s** | **int** | The bid size. This represents the number of round lot orders at the given bid price. The normal round lot size is 100 shares. A bid size of 2 means there are 200 shares for purchase at the given bid price. | [optional] 
**x** | **object** |  | [optional] 
**p** | **float** | The ask price. | [optional] 
**s** | **int** | The ask size. This represents the number of round lot orders at the given ask price. The normal round lot size is 100 shares. An ask size of 2 means there are 200 shares available to purchase at the given ask price. | [optional] 
**x** | **object** |  | [optional] 
**z** | **int** | There are 3 tapes which define which exchange the ticker is listed on. These are integers in our objects which represent the letter of the alphabet. Eg: 1 &#x3D; A, 2 &#x3D; B, 3 &#x3D; C. * Tape A is NYSE listed securities * Tape B is NYSE ARCA / NYSE American * Tape C is NASDAQ  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


