# TickerTypesResultsIndexTypes


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **str** | Index | [optional] 
**etf** | **str** | Exchange Traded Fund (ETF) | [optional] 
**etn** | **str** | Exchange Traded Note (ETN) | [optional] 
**etmf** | **str** | Exchange Traded Managed Fund (ETMF) | [optional] 
**settlement** | **str** | Settlement | [optional] 
**spot** | **str** | Spot | [optional] 
**subprod** | **str** | Subordinated product | [optional] 
**wc** | **str** | World Currency | [optional] 
**alphaindex** | **str** | Alpha Index | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


