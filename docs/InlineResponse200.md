# InlineResponse200


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **float** | The page number of the current response. | [optional] 
**per_page** | **float** | The number of results per page. | [optional] 
**count** | **int** | The total number of results for this request. | [optional] 
**status** | **str** | The status of this request&#39;s response. | [optional] 
**tickers** | [**[InlineResponse200Tickers]**](InlineResponse200Tickers.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


