# MarketsResults


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**market** | **str** | The name of the market. | [optional] 
**desc** | **str** | A description of the market. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


