# ForexConversion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last** | [**ForexConversionLast**](ForexConversionLast.md) |  | [optional] 
**_from** | **str** | The \&quot;from\&quot; currency symbol. | [optional] 
**to** | **str** | The \&quot;to\&quot; currency symbol. | [optional] 
**initial_amount** | **float** | The amount to convert. | [optional] 
**converted** | **float** | The result of the conversion. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


