# LocalesResults


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **str** | An abbreviated country name. | [optional] 
**name** | **str** | The name of the country. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


