# InlineResponse200Tickers


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticker** | **str** | The exchange symbol that this item is traded under. | [optional] 
**name** | **str** | The name of the item. | [optional] 
**market** | **str** | The market in which this ticker participates. | [optional] 
**locale** | **str** | The locale of where this ticker is traded. | [optional] 
**currency** | **str** | The currency this ticker is traded in. | [optional] 
**active** | **bool** | Whether or not the ticker is active. False means the ticker has been delisted. | [optional] 
**primary_exch** | **str** | The listing exchange for this ticker. | [optional] 
**url** | **str** | A URL that can be used to get more detailed information about the ticker. | [optional] 
**updated** | **date** | The last time this ticker record was updated. | [optional] 
**attrs** | [**InlineResponse200Attrs**](InlineResponse200Attrs.md) |  | [optional] 
**codes** | [**InlineResponse200Codes**](InlineResponse200Codes.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


