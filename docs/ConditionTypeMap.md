# ConditionTypeMap


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**condition** | **str** | Polygon.io&#39;s mapping for condition codes.  For more information, see our &lt;a href&#x3D;\&quot;https://polygon.io/glossary/us/stocks/trade-conditions\&quot; alt&#x3D;\&quot;Trade Conditions Glossary\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Trade Conditions Glossary&lt;/a&gt;.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


