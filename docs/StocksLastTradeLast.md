# StocksLastTradeLast


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **float** | The price of the trade. This is the actual dollar value per whole share of this trade. A trade of 100 shares with a price of $2.00 would be worth a total dollar value of $200.00.  | [optional] 
**size** | **float** | The size of a trade (also known as volume).  | [optional] 
**exchange** | **int** | The exchange ID. See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_exchanges_anchor\&quot; alt&#x3D;\&quot;Exchanges\&quot;&gt;Exchanges&lt;/a&gt; for Polygon.io&#39;s mapping of exchange IDs. | [optional] 
**cond1** | **int** | Condition 1 of the trade. See Polygon.io&#39;s &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_conditions__ticktype__anchor\&quot; alt&#x3D;\&quot;Mapping of conditions to exchange descriptions\&quot;&gt;mapping of conditions to exchange descriptions&lt;/a&gt;. | [optional] 
**cond2** | **int** | Condition 2 of the trade. See Polygon.io&#39;s &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_conditions__ticktype__anchor\&quot; alt&#x3D;\&quot;Mapping of conditions to exchange descriptions\&quot;&gt;mapping of conditions to exchange descriptions&lt;/a&gt;. | [optional] 
**cond3** | **int** | Condition 3 of the trade. See Polygon.io&#39;s &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_conditions__ticktype__anchor\&quot; alt&#x3D;\&quot;Mapping of conditions to exchange descriptions\&quot;&gt;mapping of conditions to exchange descriptions&lt;/a&gt;. | [optional] 
**cond4** | **int** | Condition 4 of the trade. See Polygon.io&#39;s &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_conditions__ticktype__anchor\&quot; alt&#x3D;\&quot;Mapping of conditions to exchange descriptions\&quot;&gt;mapping of conditions to exchange descriptions&lt;/a&gt;. | [optional] 
**timestamp** | **int** | The Unix Msec timestamp for the start of the aggregate window. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


