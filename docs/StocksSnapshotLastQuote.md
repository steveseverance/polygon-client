# StocksSnapshotLastQuote


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**p** | **float** | The bid price. | [optional] 
**s** | **int** | The bid size in lots. | [optional] 
**p** | **float** | The ask price. | [optional] 
**s** | **int** | The ask size in lots. | [optional] 
**t** | **int** | The Unix Msec timestamp for the start of the aggregate window. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


