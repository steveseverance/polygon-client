# Company


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logo** | **str** | The URL of the entity&#39;s logo. | [optional] 
**exchange** | **str** | The symbol&#39;s primary exchange. | [optional] 
**exchange_symbol** | **str** | The exchange code (id) of the symbol&#39;s primary exchange. | [optional] 
**type** | **str** | The type or class of the security.  (&lt;a alt&#x3D;\&quot;Full List of Ticker Types\&quot; href&#x3D;\&quot;https://polygon.io/docs/get_v2_reference_types_anchor\&quot;&gt;Full List of Ticker Types&lt;/a&gt;) | [optional] 
**name** | **str** | The name of the company/entity. | [optional] 
**symbol** | **str** | The exchange symbol that this item is traded under. | [optional] 
**listdate** | **date** | The date that the symbol was listed on the exchange. | [optional] 
**cik** | **str** | The official CIK guid used for SEC database/filings. | [optional] 
**bloomberg** | **str** | The Bloomberg guid for the symbol. | [optional] 
**figi** | **str** | The OpenFigi project guid for the symbol. (&lt;a rel&#x3D;\&quot;nofollow\&quot; target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;https://openfigi.com/\&quot;&gt;https://openfigi.com/&lt;/a&gt;) | [optional] 
**lei** | **str** | The Legal Entity Identifier (LEI) guid for the symbol. (&lt;a rel&#x3D;\&quot;nofollow\&quot; target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;https://en.wikipedia.org/wiki/Legal_Entity_Identifier\&quot;&gt;https://en.wikipedia.org/wiki/Legal_Entity_Identifier&lt;/a&gt;) | [optional] 
**sic** | **int** | Standard Industrial Classification (SIC) id for the symbol. (&lt;a rel&#x3D;\&quot;nofollow\&quot; target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;https://en.wikipedia.org/wiki/Standard_Industrial_Classification\&quot;&gt;https://en.wikipedia.org/wiki/Legal_Entity_Identifier&lt;/a&gt;) | [optional] 
**country** | **str** | The country in which the company is registered. | [optional] 
**industry** | **str** | The industry in which the company operates. | [optional] 
**sector** | **str** | The sector of the indsutry in which the symbol operates. | [optional] 
**marketcap** | **int** | The current market cap for the company. | [optional] 
**employees** | **int** | The approximate number of employees for the company. | [optional] 
**phone** | **str** | The phone number for the company. This is usually a corporate contact number. | [optional] 
**ceo** | **str** | The name of the company&#39;s current CEO. | [optional] 
**url** | **str** | The URL of the company&#39;s website | [optional] 
**description** | **str** | A description of the company and what they do/offer. | [optional] 
**hq_address** | **str** | The street address for the company&#39;s headquarters. | [optional] 
**hq_state** | **str** | The state in which the company&#39;s headquarters is located. | [optional] 
**hq_country** | **str** | The country in which the company&#39;s headquarters is located. | [optional] 
**similar** | **[str]** | A list of ticker symbols for similar companies. | [optional] 
**tags** | **[str]** |  | [optional] 
**updated** | **date** | The last time this company record was updated. | [optional] 
**active** | **bool** | Indicates if the security is actively listed.  If false, this means the company is no longer listed and cannot be traded. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


