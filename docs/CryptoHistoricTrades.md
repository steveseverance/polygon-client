# CryptoHistoricTrades


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **date** | The date that was evaluated from the request. | [optional] 
**map** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | A map for shortened result keys. | [optional] 
**ms_latency** | **int** | The milliseconds of latency for the query results. | [optional] 
**symbol** | **str** | The symbol pair that was evaluated from the request. | [optional] 
**ticks** | [**[InlineResponse20010OpenTrades]**](InlineResponse20010OpenTrades.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


