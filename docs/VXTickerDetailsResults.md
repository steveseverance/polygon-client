# VXTickerDetailsResults

Ticker with details. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticker** | **str** | The exchange symbol that this item is traded under. | [optional] 
**name** | **str** | The name of the asset. For stocks/equities this will be the companies registered name. For crypto/fx this will be the name of the currency or coin pair.  | [optional] 
**market** | **str** | The market type of the asset. | [optional] 
**locale** | **str** | The locale of the asset. | [optional] 
**primary_exchange** | **str** | The ISO code of the primary listing exchange for this asset. | [optional] 
**type** | **str** | The type of the asset. Find the types that we support via our [Ticker Types API](https://polygon.io/docs/get_v2_reference_types_anchor). | [optional] 
**active** | **bool** | Whether or not the asset is actively traded. False means the asset has been delisted. | [optional] 
**currency_name** | **str** | The name of the currency that this asset is traded with. | [optional] 
**cik** | **str** | The CIK number for this ticker. Find more information [here](https://en.wikipedia.org/wiki/Central_Index_Key). | [optional] 
**composite_figi** | **str** | The composite OpenFIGI number for this ticker. Find more information [here](https://www.openfigi.com/assets/content/Open_Symbology_Fields-2a61f8aa4d.pdf) | [optional] 
**share_class_figi** | **str** | The share Class OpenFIGI number for this ticker. Find more information [here](https://www.openfigi.com/assets/content/Open_Symbology_Fields-2a61f8aa4d.pdf) | [optional] 
**last_updated_utc** | **datetime** | The last time this asset record was updated. | [optional] 
**delisted_utc** | **datetime** | The last date that the asset was traded. | [optional] 
**outstanding_shares** | **float** | The recorded number of outstanding shares. | [optional] 
**market_cap** | **float** | The most recent close price of the ticker multiplied by outstanding shares. | [optional] 
**phone_number** | **str** | The phone number for the company behind this ticker. | [optional] 
**address** | [**VXTickerDetailsResultsAddress**](VXTickerDetailsResultsAddress.md) |  | [optional] 
**sic_code** | **str** | The standard industrial classification code for this ticker.  For a list of SIC Codes, see the SEC&#39;s &lt;a rel&#x3D;\&quot;nofollow\&quot; target&#x3D;\&quot;_blank\&quot; href&#x3D;\&quot;https://www.sec.gov/info/edgar/siccodes.htm\&quot;&gt;SIC Code List&lt;/a&gt;.  | [optional] 
**sic_description** | **str** | A description of this ticker&#39;s SIC code. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


