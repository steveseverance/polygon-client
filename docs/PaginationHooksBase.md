# PaginationHooksBase


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_page_path** | **str** | If present, this value can be used to fetch the next page of data by appending this to the API url host. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


