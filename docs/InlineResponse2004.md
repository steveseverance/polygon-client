# InlineResponse2004


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exchange** | **str** | Which market the record is for. | [optional] 
**name** | **str** | The name of the holiday. | [optional] 
**status** | **str** | The status of the market on the holiday. | [optional] 
**date** | **date** | The date of the holiday. | [optional] 
**open** | **datetime** | The market open time on the holiday (if it&#39;s not closed). | [optional] 
**close** | **datetime** | The market close time on the holiday (if it&#39;s not closed). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


