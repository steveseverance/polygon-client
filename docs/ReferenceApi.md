# openapi_client.ReferenceApi

All URIs are relative to *https://api.polygon.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1_marketstatus_now_get**](ReferenceApi.md#v1_marketstatus_now_get) | **GET** /v1/marketstatus/now | Market Status
[**v1_marketstatus_upcoming_get**](ReferenceApi.md#v1_marketstatus_upcoming_get) | **GET** /v1/marketstatus/upcoming | Market Holidays
[**v1_meta_symbols_stocks_ticker_company_get**](ReferenceApi.md#v1_meta_symbols_stocks_ticker_company_get) | **GET** /v1/meta/symbols/{stocksTicker}/company | Ticker Details
[**v1_meta_symbols_stocks_ticker_news_get**](ReferenceApi.md#v1_meta_symbols_stocks_ticker_news_get) | **GET** /v1/meta/symbols/{stocksTicker}/news | Ticker News
[**v2_reference_dividends_stocks_ticker_get**](ReferenceApi.md#v2_reference_dividends_stocks_ticker_get) | **GET** /v2/reference/dividends/{stocksTicker} | Stock Dividends
[**v2_reference_financials_stocks_ticker_get**](ReferenceApi.md#v2_reference_financials_stocks_ticker_get) | **GET** /v2/reference/financials/{stocksTicker} | Stock Financials
[**v2_reference_locales_get**](ReferenceApi.md#v2_reference_locales_get) | **GET** /v2/reference/locales | Locales
[**v2_reference_markets_get**](ReferenceApi.md#v2_reference_markets_get) | **GET** /v2/reference/markets | Markets
[**v2_reference_splits_stocks_ticker_get**](ReferenceApi.md#v2_reference_splits_stocks_ticker_get) | **GET** /v2/reference/splits/{stocksTicker} | Stock Splits
[**v2_reference_tickers_get**](ReferenceApi.md#v2_reference_tickers_get) | **GET** /v2/reference/tickers | Tickers
[**v2_reference_types_get**](ReferenceApi.md#v2_reference_types_get) | **GET** /v2/reference/types | Ticker Types
[**v_x_reference_tickers_get**](ReferenceApi.md#v_x_reference_tickers_get) | **GET** /vX/reference/tickers | Tickers vX
[**v_x_reference_tickers_ticker_get**](ReferenceApi.md#v_x_reference_tickers_ticker_get) | **GET** /vX/reference/tickers/{ticker} | Ticker Details vX


# **v1_marketstatus_now_get**
> InlineResponse2005 v1_marketstatus_now_get()

Market Status

Get the current trading status of the exchanges and overall financial markets. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from openapi_client.model.inline_response2005 import InlineResponse2005
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Market Status
        api_response = api_instance.v1_marketstatus_now_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v1_marketstatus_now_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Status of the market and each exchange |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_marketstatus_upcoming_get**
> [InlineResponse2004] v1_marketstatus_upcoming_get()

Market Holidays

Get upcoming market holidays and their open/close times. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from openapi_client.model.inline_response2004 import InlineResponse2004
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Market Holidays
        api_response = api_instance.v1_marketstatus_upcoming_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v1_marketstatus_upcoming_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[InlineResponse2004]**](InlineResponse2004.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Holidays for each market in the near future. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_meta_symbols_stocks_ticker_company_get**
> InlineResponse2001 v1_meta_symbols_stocks_ticker_company_get(stocks_ticker)

Ticker Details

Get details for a ticker symbol's company/entity.  This provides  a general overview of the entity with information such as name,  sector, exchange, logo and similar companies. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from openapi_client.model.inline_response2001 import InlineResponse2001
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Ticker Details
        api_response = api_instance.v1_meta_symbols_stocks_ticker_company_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v1_meta_symbols_stocks_ticker_company_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Company |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_meta_symbols_stocks_ticker_news_get**
> [InlineResponse2002] v1_meta_symbols_stocks_ticker_news_get(stocks_ticker)

Ticker News

Get the most recent news articles relating to a stock ticker symbol, including a summary of the article and a link to the original source. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from openapi_client.model.inline_response2002 import InlineResponse2002
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.
    perpage = 50 # int | The maximum number of results to be returned on each page, max 50 and default 50.  (optional)
    page = 1 # int | Which page of results to return.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Ticker News
        api_response = api_instance.v1_meta_symbols_stocks_ticker_news_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v1_meta_symbols_stocks_ticker_news_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Ticker News
        api_response = api_instance.v1_meta_symbols_stocks_ticker_news_get(stocks_ticker, perpage=perpage, page=page)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v1_meta_symbols_stocks_ticker_news_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |
 **perpage** | **int**| The maximum number of results to be returned on each page, max 50 and default 50.  | [optional]
 **page** | **int**| Which page of results to return.  | [optional]

### Return type

[**[InlineResponse2002]**](InlineResponse2002.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of news articles. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_dividends_stocks_ticker_get**
> object v2_reference_dividends_stocks_ticker_get(stocks_ticker)

Stock Dividends

Get a list of historical dividends for a stock, including the relevant dates and the amount of the dividend. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Stock Dividends
        api_response = api_instance.v2_reference_dividends_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_dividends_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of historical dividends. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_financials_stocks_ticker_get**
> InlineResponse2003 v2_reference_financials_stocks_ticker_get(stocks_ticker)

Stock Financials

Get historical financial data for a stock ticker. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from openapi_client.model.inline_response2003 import InlineResponse2003
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.
    limit = 5 # int | Limit the number of results.  (optional)
    type = "Y" # str | Specify a type of report to return.  Y = Year YA = Year annualized Q = Quarter QA = Quarter Annualized T = Trailing twelve months TA = trailing twelve months annualized  (optional)
    sort = "reportPeriod" # str | The direction to sort the returned results.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Stock Financials
        api_response = api_instance.v2_reference_financials_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_financials_stocks_ticker_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Stock Financials
        api_response = api_instance.v2_reference_financials_stocks_ticker_get(stocks_ticker, limit=limit, type=type, sort=sort)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_financials_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |
 **limit** | **int**| Limit the number of results.  | [optional]
 **type** | **str**| Specify a type of report to return.  Y &#x3D; Year YA &#x3D; Year annualized Q &#x3D; Quarter QA &#x3D; Quarter Annualized T &#x3D; Trailing twelve months TA &#x3D; trailing twelve months annualized  | [optional]
 **sort** | **str**| The direction to sort the returned results.  | [optional]

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of financials. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_locales_get**
> object v2_reference_locales_get()

Locales

Get a list of locales currently supported by Polygon.io. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Locales
        api_response = api_instance.v2_reference_locales_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_locales_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of locales |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_markets_get**
> object v2_reference_markets_get()

Markets

Get a list of markets that are currently supported by Polygon.io. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Markets
        api_response = api_instance.v2_reference_markets_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_markets_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of supported markets. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_splits_stocks_ticker_get**
> object v2_reference_splits_stocks_ticker_get(stocks_ticker)

Stock Splits

Get a list of historical stock splits for a ticker symbol, including the execution and payment dates of the stock split, and the split ratio. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    stocks_ticker = "AAPL" # str | The ticker symbol of the stock/equity.

    # example passing only required values which don't have defaults set
    try:
        # Stock Splits
        api_response = api_instance.v2_reference_splits_stocks_ticker_get(stocks_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_splits_stocks_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stocks_ticker** | **str**| The ticker symbol of the stock/equity. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of stock splits. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_tickers_get**
> InlineResponse200 v2_reference_tickers_get()

Tickers

Query all ticker symbols which are supported by Polygon.io. Returns basic reference data for each matched ticker symbol. This API includes Crypto, Forex, and Stocks/Equities. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from openapi_client.model.inline_response200 import InlineResponse200
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    sort = "ticker" # str | Which field to sort by. For desc place a `-` in front of the field name.  **Example:** - `?sort=-ticker` to sort symbols Z-A - `?sort=type` to sort symbols by type  (optional)
    type = "type_example" # str | Get tickers of a certain type. See <a href=\"https://polygon.io/docs/get_v2_reference_types_anchor\" alt=\"Ticker Types\">Ticker Types</a> for available types.  **Example** - `?type=etp` to get all ETFs - `?type=cs` to get all Common Stocks  (optional)
    market = "STOCKS" # str | Get tickers for a specific market.  **Example:** - `?market=stocks` to get all stock tickers - `?market=crypto` to get all crypto tickers - `?market=fx` to get all forex tickers  (optional)
    locale = "locale_example" # str | Get tickers for a specific region/locale.  See <a href=\"https://polygon.io/docs/get_v2_reference_locales_anchor\" alt=\"Locales\">Locales</a> for a list of possible values.  **Example:** - `?locale=us` to get all US tickers - `?locale=g` to get all Global tickers  (optional)
    search = "search_example" # str | Search for specific tickers by name or ticker symbol.  **Example:** - `?search=microsoft` Search tickers for microsoft  (optional)
    perpage = 50 # int | The maximum number of results to be returned on each page, max 50 and default 50.  (optional)
    page = 1 # int | Which page of results to return.  (optional)
    active = True # bool | Filter for only active or inactive symbols.  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Tickers
        api_response = api_instance.v2_reference_tickers_get(sort=sort, type=type, market=market, locale=locale, search=search, perpage=perpage, page=page, active=active)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_tickers_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sort** | **str**| Which field to sort by. For desc place a &#x60;-&#x60; in front of the field name.  **Example:** - &#x60;?sort&#x3D;-ticker&#x60; to sort symbols Z-A - &#x60;?sort&#x3D;type&#x60; to sort symbols by type  | [optional]
 **type** | **str**| Get tickers of a certain type. See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v2_reference_types_anchor\&quot; alt&#x3D;\&quot;Ticker Types\&quot;&gt;Ticker Types&lt;/a&gt; for available types.  **Example** - &#x60;?type&#x3D;etp&#x60; to get all ETFs - &#x60;?type&#x3D;cs&#x60; to get all Common Stocks  | [optional]
 **market** | **str**| Get tickers for a specific market.  **Example:** - &#x60;?market&#x3D;stocks&#x60; to get all stock tickers - &#x60;?market&#x3D;crypto&#x60; to get all crypto tickers - &#x60;?market&#x3D;fx&#x60; to get all forex tickers  | [optional]
 **locale** | **str**| Get tickers for a specific region/locale.  See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v2_reference_locales_anchor\&quot; alt&#x3D;\&quot;Locales\&quot;&gt;Locales&lt;/a&gt; for a list of possible values.  **Example:** - &#x60;?locale&#x3D;us&#x60; to get all US tickers - &#x60;?locale&#x3D;g&#x60; to get all Global tickers  | [optional]
 **search** | **str**| Search for specific tickers by name or ticker symbol.  **Example:** - &#x60;?search&#x3D;microsoft&#x60; Search tickers for microsoft  | [optional]
 **perpage** | **int**| The maximum number of results to be returned on each page, max 50 and default 50.  | [optional]
 **page** | **int**| Which page of results to return.  | [optional]
 **active** | **bool**| Filter for only active or inactive symbols.  | [optional]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of symbols |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_reference_types_get**
> object v2_reference_types_get()

Ticker Types

Get a mapping of ticker types to their descriptive names. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Ticker Types
        api_response = api_instance.v2_reference_types_get()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v2_reference_types_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of types |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |
**404** | The specified resource was not found |  -  |
**409** | Parameter is invalid or incorrect. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v_x_reference_tickers_get**
> object v_x_reference_tickers_get()

Tickers vX

Query all ticker symbols which are supported by Polygon.io. This API currently includes Stocks/Equities, Crypto, and Forex. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    ticker = "ticker_example" # str | Specify a ticker symbol. Defaults to empty string which queries all tickers.  (optional)
    type = "type_example" # str | Specify the type of the tickers. Find the types that we support via our [Ticker Types API](https://polygon.io/docs/get_v2_reference_types_anchor). Defaults to empty string which queries all types.  (optional)
    market = "stocks" # str | Filter by market type. By default all markets are included.  (optional)
    exchange = "exchange_example" # str | Specify the primary exchange of the asset in the ISO code format. Find more information about the ISO codes [at the ISO org website](https://www.iso20022.org/market-identifier-codes). Defaults to empty string which queries all exchanges.  (optional)
    cusip = "cusip_example" # str | Specify the CUSIP code of the asset you want to search for. Find more information about CUSIP codes [at their website](https://www.cusip.com/identifiers.html#/CUSIP). Defaults to empty string which queries all CUSIPs.  Note: Although you can query by CUSIP, due to legal reasons we do not return the CUSIP in the response.  (optional)
    date =  # object | Specify a point in time to retrieve tickers available on that date. Defaults to the most recent available date.  (optional)
    active = True # bool | Specify if the tickers returned should be actively traded on the queried date. Default is true.  (optional)
    sort = "ticker" # str | The field to sort the results on. Default is ticker.  (optional)
    order = "asc" # str | The order to sort the results on. Default is asc (ascending).  (optional)
    limit = 10 # int | Limit the size of the response, default is 100 and max is 1000.  If your query returns more than the max limit and you want to retrieve the next page of results, see the `next_page_path` response attribute.  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Tickers vX
        api_response = api_instance.v_x_reference_tickers_get(ticker=ticker, type=type, market=market, exchange=exchange, cusip=cusip, date=date, active=active, sort=sort, order=order, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v_x_reference_tickers_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| Specify a ticker symbol. Defaults to empty string which queries all tickers.  | [optional]
 **type** | **str**| Specify the type of the tickers. Find the types that we support via our [Ticker Types API](https://polygon.io/docs/get_v2_reference_types_anchor). Defaults to empty string which queries all types.  | [optional]
 **market** | **str**| Filter by market type. By default all markets are included.  | [optional]
 **exchange** | **str**| Specify the primary exchange of the asset in the ISO code format. Find more information about the ISO codes [at the ISO org website](https://www.iso20022.org/market-identifier-codes). Defaults to empty string which queries all exchanges.  | [optional]
 **cusip** | **str**| Specify the CUSIP code of the asset you want to search for. Find more information about CUSIP codes [at their website](https://www.cusip.com/identifiers.html#/CUSIP). Defaults to empty string which queries all CUSIPs.  Note: Although you can query by CUSIP, due to legal reasons we do not return the CUSIP in the response.  | [optional]
 **date** | **object**| Specify a point in time to retrieve tickers available on that date. Defaults to the most recent available date.  | [optional]
 **active** | **bool**| Specify if the tickers returned should be actively traded on the queried date. Default is true.  | [optional]
 **sort** | **str**| The field to sort the results on. Default is ticker.  | [optional]
 **order** | **str**| The order to sort the results on. Default is asc (ascending).  | [optional]
 **limit** | **int**| Limit the size of the response, default is 100 and max is 1000.  If your query returns more than the max limit and you want to retrieve the next page of results, see the &#x60;next_page_path&#x60; response attribute.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Reference Tickers. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v_x_reference_tickers_ticker_get**
> object v_x_reference_tickers_ticker_get(ticker)

Ticker Details vX

Get a single ticker supported by Polygon.io. This response will have detailed information about the ticker and the company behind it. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import reference_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = reference_api.ReferenceApi(api_client)
    ticker = "AAPL" # str | The ticker symbol of the asset.
    date =  # object | Specify a point in time to get information about the ticker available on that date. When retrieving information from SEC filings, we compare this date with the period of report date on the SEC filing.  For example, consider an SEC filing submitted by AAPL on 2019-07-31, with a period of report date ending on 2019-06-29. That means that the filing was submitted on 2019-07-31, but the filing was created based on information from 2019-06-29. If you were to query for AAPL details on 2019-06-29, the ticker details would include information from the SEC filing.  Defaults to the most recent available date.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Ticker Details vX
        api_response = api_instance.v_x_reference_tickers_ticker_get(ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v_x_reference_tickers_ticker_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Ticker Details vX
        api_response = api_instance.v_x_reference_tickers_ticker_get(ticker, date=date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ReferenceApi->v_x_reference_tickers_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| The ticker symbol of the asset. |
 **date** | **object**| Specify a point in time to get information about the ticker available on that date. When retrieving information from SEC filings, we compare this date with the period of report date on the SEC filing.  For example, consider an SEC filing submitted by AAPL on 2019-07-31, with a period of report date ending on 2019-06-29. That means that the filing was submitted on 2019-07-31, but the filing was created based on information from 2019-06-29. If you were to query for AAPL details on 2019-06-29, the ticker details would include information from the SEC filing.  Defaults to the most recent available date.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Reference Tickers. |  -  |
**401** | Unauthorized - Check our API Key and account status |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

