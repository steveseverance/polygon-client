# ForexConversionLast


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ask** | **float** | The ask price. | [optional] 
**bid** | **float** | The bid price. | [optional] 
**exchange** | **int** | The exchange ID. See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_exchanges_anchor\&quot; alt&#x3D;\&quot;Exchanges\&quot;&gt;Exchanges&lt;/a&gt; for Polygon.io&#39;s mapping of exchange IDs. | [optional] 
**timestamp** | **int** | The Unix Msec timestamp for the start of the aggregate window. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


