# Conditions

A list of condition codes. 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **[int]** | A list of condition codes.  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


