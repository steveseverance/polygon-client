# StocksV2Trade


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**t** | **str** | The exchange symbol that this item is traded under. | [optional] 
**t** | **int** | The nanosecond accuracy SIP Unix Timestamp. This is the timestamp of when the SIP received this message from the exchange which produced it. | [optional] 
**y** | **int** | The nanosecond accuracy Participant/Exchange Unix Timestamp. This is the timestamp of when the quote was actually generated at the exchange. | [optional] 
**f** | **int** | The nanosecond accuracy TRF(Trade Reporting Facility) Unix Timestamp. This is the timestamp of when the trade reporting facility received this message. | [optional] 
**q** | **int** | The sequence number represents the sequence in which message events happened. These are increasing and unique per ticker symbol, but will not always be sequential (e.g., 1, 2, 6, 9, 10, 11).  | [optional] 
**c** | **[int]** | A list of condition codes.  | [optional] 
**i** | **int** | The Trade ID which uniquely identifies a trade. These are unique per combination of ticker, exchange, and TRF. For example: A trade for AAPL executed on NYSE and a trade for AAPL executed on NASDAQ could potentially have the same Trade ID.  | [optional] 
**p** | **float** | The price of the trade. This is the actual dollar value per whole share of this trade. A trade of 100 shares with a price of $2.00 would be worth a total dollar value of $200.00.  | [optional] 
**s** | **float** | The size of a trade (also known as volume).  | [optional] 
**e** | **int** | The trade correction indicator.  | [optional] 
**x** | **int** | The exchange ID. See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_exchanges_anchor\&quot; alt&#x3D;\&quot;Exchanges\&quot;&gt;Exchanges&lt;/a&gt; for Polygon.io&#39;s mapping of exchange IDs. | [optional] 
**r** | **int** | The ID for the Trade Reporting Facility where the trade took place.  | [optional] 
**z** | **int** | There are 3 tapes which define which exchange the ticker is listed on. These are integers in our objects which represent the letter of the alphabet. Eg: 1 &#x3D; A, 2 &#x3D; B, 3 &#x3D; C. * Tape A is NYSE listed securities * Tape B is NYSE ARCA / NYSE American * Tape C is NASDAQ  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


