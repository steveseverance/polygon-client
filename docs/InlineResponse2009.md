# InlineResponse2009


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The exchange ID. See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_exchanges_anchor\&quot; alt&#x3D;\&quot;Exchanges\&quot;&gt;Exchanges&lt;/a&gt; for Polygon.io&#39;s mapping of exchange IDs. | [optional] 
**type** | **str** | Type of exchange feed | [optional] 
**market** | **str** | Market data type this exchange contains ( crypto only currently ) | [optional] 
**name** | **str** | Name of the exchange | [optional] 
**url** | **str** | URL of this exchange | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


