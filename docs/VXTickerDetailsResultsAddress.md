# VXTickerDetailsResultsAddress


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address1** | **str** | The first line of the company&#39;s headquarters address. | [optional] 
**city** | **str** | The city of the company&#39;s headquarters address. | [optional] 
**state** | **str** | The state of the company&#39;s headquarters address. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


