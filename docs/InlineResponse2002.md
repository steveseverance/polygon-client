# InlineResponse2002


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**symbols** | **[str]** | A list of ticker symbols relating to the article. | [optional] 
**title** | **str** | The title of the news article. | [optional] 
**url** | **str** | A direct link to the news article from its source publication. | [optional] 
**source** | **str** | The publication source of the article. | [optional] 
**summary** | **str** | A summary of the news article. | [optional] 
**image** | **str** | A URL of the image for the news article, if found. | [optional] 
**timestamp** | **datetime** | The timestamp of the news article. | [optional] 
**keywords** | **[str]** | A list of common keywords related to the news article. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


