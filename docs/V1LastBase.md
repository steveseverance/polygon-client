# V1LastBase


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**symbol** | **str** | The exchange symbol that this item is traded under. | [optional] 
**status** | **str** | The status of this request&#39;s response. | [optional] 
**request_id** | **str** | A request id assigned by the server. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


