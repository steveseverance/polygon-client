# CryptoLastTradeLast


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conditions** | **[int]** | A list of condition codes.  | [optional] 
**exchange** | **int** | The exchange that this crypto trade happened on.   See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_crypto-exchanges_anchor\&quot;&gt;Crypto Exchanges&lt;/a&gt; for a mapping of exchanges to IDs.  | [optional] 
**price** | **float** | The price of the trade. This is the actual dollar value per whole share of this trade. A trade of 100 shares with a price of $2.00 would be worth a total dollar value of $200.00.  | [optional] 
**size** | **float** | The size of a trade (also known as volume).  | [optional] 
**timestamp** | **int** | The Unix Msec timestamp for the start of the aggregate window. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


