# StocksSnapshotTickersLastTrade


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c** | **[str]** | The trade conditions. | [optional] 
**i** | **int** | The Trade ID which uniquely identifies a trade. These are unique per combination of ticker, exchange, and TRF. For example: A trade for AAPL executed on NYSE and a trade for AAPL executed on NASDAQ could potentially have the same Trade ID.  | [optional] 
**p** | **float** | The price of the trade. This is the actual dollar value per whole share of this trade. A trade of 100 shares with a price of $2.00 would be worth a total dollar value of $200.00.  | [optional] 
**s** | **int** | The size (volume) of the trade. | [optional] 
**t** | **int** | The Unix Msec timestamp for the start of the aggregate window. | [optional] 
**x** | **int** | The exchange ID. See &lt;a href&#x3D;\&quot;https://polygon.io/docs/get_v1_meta_exchanges_anchor\&quot; alt&#x3D;\&quot;Exchanges\&quot;&gt;Exchanges&lt;/a&gt; for Polygon.io&#39;s mapping of exchange IDs. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


