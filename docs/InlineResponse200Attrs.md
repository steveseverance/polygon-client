# InlineResponse200Attrs


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency_name** | **str** | The name of the variable/quote currency. | [optional] 
**currency** | **str** | The currency code of the variable/quote currency. | [optional] 
**base_name** | **str** | The name of the base currency. | [optional] 
**base** | **str** | The currency code of the base currency. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


