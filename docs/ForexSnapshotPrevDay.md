# ForexSnapshotPrevDay


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**o** | **float** | The open price for the symbol in the given time period. | [optional] 
**h** | **float** | The highest price for the symbol in the given time period. | [optional] 
**l** | **float** | The lowest price for the symbol in the given time period. | [optional] 
**c** | **float** | The close price for the symbol in the given time period. | [optional] 
**v** | **float** | The trading volume of the symbol in the given time period. | [optional] 
**vw** | **float** | The volume weighted average price. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


