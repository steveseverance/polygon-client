# Financial


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**symbol** | **str** | Stock Symbol | 
**report_date** | **datetime** | Report Date | 
**report_date_str** | **str** | Report date as non date format | 
**gross_profit** | **int** |  | [optional] 
**cost_of_revenue** | **int** |  | [optional] 
**operating_revenue** | **int** |  | [optional] 
**total_revenue** | **int** |  | [optional] 
**operating_income** | **int** |  | [optional] 
**net_income** | **int** |  | [optional] 
**research_and_development** | **int** |  | [optional] 
**operating_expense** | **int** |  | [optional] 
**current_assets** | **int** |  | [optional] 
**total_assets** | **int** |  | [optional] 
**total_liabilities** | **int** |  | [optional] 
**current_cash** | **int** |  | [optional] 
**current_debt** | **int** |  | [optional] 
**total_cash** | **int** |  | [optional] 
**total_debt** | **int** |  | [optional] 
**shareholder_equity** | **int** |  | [optional] 
**cash_change** | **int** |  | [optional] 
**cash_flow** | **int** |  | [optional] 
**operating_gains_losses** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


