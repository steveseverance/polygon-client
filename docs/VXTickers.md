# VXTickers


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[VXTickersResults]**](VXTickersResults.md) | An array of tickers that match your query.  Note: Although you can query by CUSIP, due to legal reasons we do not return the CUSIP in the response.                                                                                                                                                                                                                                                                                                     | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


