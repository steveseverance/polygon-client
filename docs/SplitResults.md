# SplitResults


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticker** | **str** | The exchange symbol that this item is traded under. | [optional] 
**ex_date** | **date** | The execution date of the stock split. | [optional] 
**payment_date** | **date** | The payment date of the stock split. | [optional] 
**declared_date** | **date** | The declared date of the stock split. | [optional] 
**ratio** | **float** | This referst to the split ratio.  The split ratio is an inverse of the number of shares that a holder of the stock would have after the split divided by the number of shares that the holder had before.  For example: A split ratio of .5 means a 2 for 1 split.  | [optional] 
**tofactor** | **int** | The \&quot;to\&quot; factor of the split. This is used to calculate the split ratio forfactor/tofactor &#x3D; ratio (eg ½ &#x3D; 0.5).  | [optional] 
**forfactor** | **int** | The \&quot;for\&quot; factor of the split. This is used to calculate the split ratio forfactor/tofactor &#x3D; ratio (eg ½ &#x3D; 0.5).  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


