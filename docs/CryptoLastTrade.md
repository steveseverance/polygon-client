# CryptoLastTrade


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last** | [**CryptoLastTradeLast**](CryptoLastTradeLast.md) |  | [optional] 
**symbol** | **str** | The symbol pair that was evaluated from the request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


