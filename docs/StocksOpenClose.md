# StocksOpenClose


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The status of this request&#39;s response. | [optional] 
**_from** | **date** | The requested date. | [optional] 
**symbol** | **str** | The exchange symbol that this item is traded under. | [optional] 
**open** | **float** | The open price for the symbol in the given time period. | [optional] 
**high** | **float** | The highest price for the symbol in the given time period. | [optional] 
**low** | **float** | The lowest price for the symbol in the given time period. | [optional] 
**close** | **float** | The close price for the symbol in the given time period. | [optional] 
**volume** | **float** | The trading volume of the symbol in the given time period. | [optional] 
**pre_market** | **int** | The open price of the ticker symbol in pre-market trading. | [optional] 
**after_hours** | **float** | The close price of the ticker symbol in after hours trading. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


