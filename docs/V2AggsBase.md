# V2AggsBase


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The status of this request&#39;s response. | [optional] 
**adjusted** | **bool** | Whether or not this response was adjusted for splits. | [optional] 
**query_count** | **int** | The number of aggregates (minute or day) used to generate the response. | [optional] 
**results_count** | **int** | The total number of results for this request. | [optional] 
**request_id** | **str** | A request id assigned by the server. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


