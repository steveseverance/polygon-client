# InlineResponse2005Currencies


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fx** | **str** | The status of the forex market. | [optional] 
**crypto** | **str** | The status of the crypto market. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


