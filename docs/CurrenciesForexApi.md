# openapi_client.CurrenciesForexApi

All URIs are relative to *https://api.polygon.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1_conversion_from_to_get**](CurrenciesForexApi.md#v1_conversion_from_to_get) | **GET** /v1/conversion/{from}/{to} | Real-time Currency Conversion
[**v1_historic_forex_from_to_date_get**](CurrenciesForexApi.md#v1_historic_forex_from_to_date_get) | **GET** /v1/historic/forex/{from}/{to}/{date} | Historic Forex Ticks
[**v1_last_quote_currencies_from_to_get**](CurrenciesForexApi.md#v1_last_quote_currencies_from_to_get) | **GET** /v1/last_quote/currencies/{from}/{to} | Last Quote for a Currency Pair
[**v2_aggs_ticker_forex_ticker_prev_get**](CurrenciesForexApi.md#v2_aggs_ticker_forex_ticker_prev_get) | **GET** /v2/aggs/ticker/{forexTicker}/prev | Previous Close
[**v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get**](CurrenciesForexApi.md#v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get) | **GET** /v2/aggs/ticker/{forexTicker}/range/{multiplier}/{timespan}/{from}/{to} | Aggregates (Bars)
[**v2_snapshot_locale_global_markets_forex_direction_get**](CurrenciesForexApi.md#v2_snapshot_locale_global_markets_forex_direction_get) | **GET** /v2/snapshot/locale/global/markets/forex/{direction} | Snapshot - Gainers/Losers
[**v2_snapshot_locale_global_markets_forex_tickers_get**](CurrenciesForexApi.md#v2_snapshot_locale_global_markets_forex_tickers_get) | **GET** /v2/snapshot/locale/global/markets/forex/tickers | Snapshot - All Tickers
[**v2_snapshot_locale_global_markets_forex_tickers_ticker_get**](CurrenciesForexApi.md#v2_snapshot_locale_global_markets_forex_tickers_ticker_get) | **GET** /v2/snapshot/locale/global/markets/forex/tickers/{ticker} | Snapshot - Ticker


# **v1_conversion_from_to_get**
> object v1_conversion_from_to_get(_from, to)

Real-time Currency Conversion

Get currency conversions using the latest market conversion rates. Note than you can convert in both directions. For example USD to CAD or CAD to USD. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    _from = "AUD" # str | The \"from\" symbol of the pair.
    to = "USD" # str | The \"to\" symbol of the pair.
    amount = 100 # int | The amount to convert, with a decimal. (optional)
    precision = 2 # int | The decimal precision of the conversion. Defaults to 2 which is 2 decimal places accuracy. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Real-time Currency Conversion
        api_response = api_instance.v1_conversion_from_to_get(_from, to)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v1_conversion_from_to_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Real-time Currency Conversion
        api_response = api_instance.v1_conversion_from_to_get(_from, to, amount=amount, precision=precision)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v1_conversion_from_to_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| The \&quot;from\&quot; symbol of the pair. |
 **to** | **str**| The \&quot;to\&quot; symbol of the pair. |
 **amount** | **int**| The amount to convert, with a decimal. | [optional]
 **precision** | **int**| The decimal precision of the conversion. Defaults to 2 which is 2 decimal places accuracy. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last tick for this currency pair, plus the converted amount for the requested amount. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_historic_forex_from_to_date_get**
> object v1_historic_forex_from_to_date_get(_from, to, date)

Historic Forex Ticks

Get historic ticks for a forex currency pair. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    _from = "AUD" # str | The \"from\" symbol of the currency pair.  Example: For **USD/JPY** the `from` would be **USD**. 
    to = "USD" # str | The \"to\" symbol of the currency pair.  Example: For **USD/JPY** the `to` would be **JPY**. 
    date = dateutil_parser('2020-10-14').date() # date | The date/day of the historic ticks to retrieve.
    offset = 1 # int | The timestamp offset, used for pagination. This is the offset at which to start the results. Using the `timestamp` of the last result as the offset will give you the next page of results.  (optional)
    limit = 100 # int | Limit the size of the response, max 10000. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Historic Forex Ticks
        api_response = api_instance.v1_historic_forex_from_to_date_get(_from, to, date)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v1_historic_forex_from_to_date_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Historic Forex Ticks
        api_response = api_instance.v1_historic_forex_from_to_date_get(_from, to, date, offset=offset, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v1_historic_forex_from_to_date_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| The \&quot;from\&quot; symbol of the currency pair.  Example: For **USD/JPY** the &#x60;from&#x60; would be **USD**.  |
 **to** | **str**| The \&quot;to\&quot; symbol of the currency pair.  Example: For **USD/JPY** the &#x60;to&#x60; would be **JPY**.  |
 **date** | **date**| The date/day of the historic ticks to retrieve. |
 **offset** | **int**| The timestamp offset, used for pagination. This is the offset at which to start the results. Using the &#x60;timestamp&#x60; of the last result as the offset will give you the next page of results.  | [optional]
 **limit** | **int**| Limit the size of the response, max 10000. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An array of forex ticks |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1_last_quote_currencies_from_to_get**
> object v1_last_quote_currencies_from_to_get(_from, to)

Last Quote for a Currency Pair

Get the last quote tick for a forex currency pair. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    _from = "AUD" # str | The \"from\" symbol of the pair.
    to = "USD" # str | The \"to\" symbol of the pair.

    # example passing only required values which don't have defaults set
    try:
        # Last Quote for a Currency Pair
        api_response = api_instance.v1_last_quote_currencies_from_to_get(_from, to)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v1_last_quote_currencies_from_to_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| The \&quot;from\&quot; symbol of the pair. |
 **to** | **str**| The \&quot;to\&quot; symbol of the pair. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last quote tick for this currency pair. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_ticker_forex_ticker_prev_get**
> object v2_aggs_ticker_forex_ticker_prev_get(forex_ticker)

Previous Close

Get the previous day's open, high, low, and close (OHLC) for the specified forex pair. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    forex_ticker = "C:EURUSD" # str | The ticker symbol of the currency pair.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Previous Close
        api_response = api_instance.v2_aggs_ticker_forex_ticker_prev_get(forex_ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_aggs_ticker_forex_ticker_prev_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Previous Close
        api_response = api_instance.v2_aggs_ticker_forex_ticker_prev_get(forex_ticker, unadjusted=unadjusted)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_aggs_ticker_forex_ticker_prev_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forex_ticker** | **str**| The ticker symbol of the currency pair. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The previous day OHLC for the ticker. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get**
> object v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get(forex_ticker, multiplier, timespan, _from, to)

Aggregates (Bars)

Get aggregate bars for a forex pair over a given date range in custom time window sizes. <br /> <br /> For example, if timespan = ‘minute’ and multiplier = ‘5’ then 5-minute bars will be returned. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    forex_ticker = "C:EURUSD" # str | The ticker symbol of the currency pair.
    multiplier = 1 # int | The size of the timespan multiplier.
    timespan = "day" # str | The size of the time window.
    _from = "2020-10-14" # str | The start of the aggregate time window.
    to = "2020-10-14" # str | The end of the aggregate time window.
    unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)
    sort = "asc" # str | Sort the results by timestamp. `asc` will return results in ascending order (oldest at the top), `desc` will return results in descending order (newest at the top).  (optional)
    limit = 120 # int | Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  <a href=\"https://polygon.io/blog/aggs-api-updates/\" target=\"_blank\" alt=\"Aggregate Data API Improvements\">Aggregate Data API Improvements</a>.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get(forex_ticker, multiplier, timespan, _from, to)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get(forex_ticker, multiplier, timespan, _from, to, unadjusted=unadjusted, sort=sort, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forex_ticker** | **str**| The ticker symbol of the currency pair. |
 **multiplier** | **int**| The size of the timespan multiplier. |
 **timespan** | **str**| The size of the time window. |
 **_from** | **str**| The start of the aggregate time window. |
 **to** | **str**| The end of the aggregate time window. |
 **unadjusted** | **bool**| Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  | [optional]
 **sort** | **str**| Sort the results by timestamp. &#x60;asc&#x60; will return results in ascending order (oldest at the top), &#x60;desc&#x60; will return results in descending order (newest at the top).  | [optional]
 **limit** | **int**| Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  &lt;a href&#x3D;\&quot;https://polygon.io/blog/aggs-api-updates/\&quot; target&#x3D;\&quot;_blank\&quot; alt&#x3D;\&quot;Aggregate Data API Improvements\&quot;&gt;Aggregate Data API Improvements&lt;/a&gt;.  | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Forex Aggregates. |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_forex_direction_get**
> object v2_snapshot_locale_global_markets_forex_direction_get(direction)

Snapshot - Gainers/Losers

Get the current top 20 gainers or losers of the day in forex markets. <br /> <br /> Top gainers are those tickers whose price has increased by the highest percentage since the previous day's close. Top losers are those tickers whose price has decreased by the highest percentage since the previous day's close. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    direction = "gainers" # str | The direction of the snapshot results to return. 

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Gainers/Losers
        api_response = api_instance.v2_snapshot_locale_global_markets_forex_direction_get(direction)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_snapshot_locale_global_markets_forex_direction_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **direction** | **str**| The direction of the snapshot results to return.  |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get the current gainers / losers of the day |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_forex_tickers_get**
> object v2_snapshot_locale_global_markets_forex_tickers_get()

Snapshot - All Tickers

Get the current minute, day, and previous day’s aggregate, as well as the last trade and quote for all traded forex symbols. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. This can happen as early as 4am EST. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    tickers = [
        "tickers_example",
    ] # [str] | A comma separated list of tickers to get snapshots for. (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Snapshot - All Tickers
        api_response = api_instance.v2_snapshot_locale_global_markets_forex_tickers_get(tickers=tickers)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_snapshot_locale_global_markets_forex_tickers_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tickers** | **[str]**| A comma separated list of tickers to get snapshots for. | [optional]

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current state for all tickers |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2_snapshot_locale_global_markets_forex_tickers_ticker_get**
> object v2_snapshot_locale_global_markets_forex_tickers_ticker_get(ticker)

Snapshot - Ticker

Get the current minute, day, and previous day’s aggregate, as well as the last trade and quote for a single traded currency symbol. <br /> <br /> Note: Snapshot data is cleared at 12am EST and gets populated as data is received from the exchanges. 

### Example

* Api Key Authentication (apiKey):
```python
import time
import openapi_client
from openapi_client.api import currencies___forex_api
from pprint import pprint
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = currencies___forex_api.CurrenciesForexApi(api_client)
    ticker = "C:EURUSD" # str | The forex ticker.

    # example passing only required values which don't have defaults set
    try:
        # Snapshot - Ticker
        api_response = api_instance.v2_snapshot_locale_global_markets_forex_tickers_ticker_get(ticker)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CurrenciesForexApi->v2_snapshot_locale_global_markets_forex_tickers_ticker_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticker** | **str**| The forex ticker. |

### Return type

**object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get current state for a ticker |  -  |
**0** | Unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

