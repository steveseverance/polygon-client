# openapi-client
The future of fintech.

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

## Requirements.

Python >= 3.6

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import openapi_client
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import openapi_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python

import time
import openapi_client
from pprint import pprint
from openapi_client.api import crypto_api
# Defining the host is optional and defaults to https://api.polygon.io
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://api.polygon.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKey
configuration.api_key['apiKey'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = crypto_api.CryptoApi(api_client)
    crypto_ticker = "X:BTCUSD" # str | The ticker symbol of the currency pair.
multiplier = 1 # int | The size of the timespan multiplier.
timespan = "day" # str | The size of the time window.
_from = "2020-10-14" # str | The start of the aggregate time window.
to = "2020-10-14" # str | The end of the aggregate time window.
unadjusted = True # bool | Whether or not the results are adjusted for splits.  By default, results are adjusted. Set this to true to get results that are NOT adjusted for splits.  (optional)
sort = "asc" # str | Sort the results by timestamp. `asc` will return results in ascending order (oldest at the top), `desc` will return results in descending order (newest at the top).  (optional)
limit = 120 # int | Limits the number of base aggregates queried to create the aggregate results. Max 50000 and Default 5000. Read more about how limit is used to calculate aggregate results in our article on  <a href=\"https://polygon.io/blog/aggs-api-updates/\" target=\"_blank\" alt=\"Aggregate Data API Improvements\">Aggregate Data API Improvements</a>.  (optional)

    try:
        # Aggregates (Bars)
        api_response = api_instance.v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get(crypto_ticker, multiplier, timespan, _from, to, unadjusted=unadjusted, sort=sort, limit=limit)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CryptoApi->v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get: %s\n" % e)
```

## Documentation for API Endpoints

All URIs are relative to *https://api.polygon.io*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CryptoApi* | [**v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get**](docs/CryptoApi.md#v2_aggs_ticker_crypto_ticker_range_multiplier_timespan_from_to_get) | **GET** /v2/aggs/ticker/{cryptoTicker}/range/{multiplier}/{timespan}/{from}/{to} | Aggregates (Bars)
*CurrenciesCryptoApi* | [**v1_historic_crypto_from_to_date_get**](docs/CurrenciesCryptoApi.md#v1_historic_crypto_from_to_date_get) | **GET** /v1/historic/crypto/{from}/{to}/{date} | Historic Crypto Trades
*CurrenciesCryptoApi* | [**v1_last_crypto_from_to_get**](docs/CurrenciesCryptoApi.md#v1_last_crypto_from_to_get) | **GET** /v1/last/crypto/{from}/{to} | Last Trade for a Crypto Pair
*CurrenciesCryptoApi* | [**v1_meta_crypto_exchanges_get**](docs/CurrenciesCryptoApi.md#v1_meta_crypto_exchanges_get) | **GET** /v1/meta/crypto-exchanges | Crypto Exchanges
*CurrenciesCryptoApi* | [**v1_open_close_crypto_from_to_date_get**](docs/CurrenciesCryptoApi.md#v1_open_close_crypto_from_to_date_get) | **GET** /v1/open-close/crypto/{from}/{to}/{date} | Daily Open/Close
*CurrenciesCryptoApi* | [**v2_aggs_grouped_locale_global_market_crypto_date_get**](docs/CurrenciesCryptoApi.md#v2_aggs_grouped_locale_global_market_crypto_date_get) | **GET** /v2/aggs/grouped/locale/global/market/crypto/{date} | Grouped Daily (Bars)
*CurrenciesCryptoApi* | [**v2_aggs_grouped_locale_global_market_fx_date_get**](docs/CurrenciesCryptoApi.md#v2_aggs_grouped_locale_global_market_fx_date_get) | **GET** /v2/aggs/grouped/locale/global/market/fx/{date} | Grouped Daily (Bars)
*CurrenciesCryptoApi* | [**v2_aggs_ticker_crypto_ticker_prev_get**](docs/CurrenciesCryptoApi.md#v2_aggs_ticker_crypto_ticker_prev_get) | **GET** /v2/aggs/ticker/{cryptoTicker}/prev | Previous Close
*CurrenciesCryptoApi* | [**v2_snapshot_locale_global_markets_crypto_direction_get**](docs/CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_direction_get) | **GET** /v2/snapshot/locale/global/markets/crypto/{direction} | Snapshot - Gainers/Losers
*CurrenciesCryptoApi* | [**v2_snapshot_locale_global_markets_crypto_tickers_get**](docs/CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_tickers_get) | **GET** /v2/snapshot/locale/global/markets/crypto/tickers | Snapshot - All Tickers
*CurrenciesCryptoApi* | [**v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get**](docs/CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_tickers_ticker_book_get) | **GET** /v2/snapshot/locale/global/markets/crypto/tickers/{ticker}/book | Snapshot - Ticker Full Book (L2)
*CurrenciesCryptoApi* | [**v2_snapshot_locale_global_markets_crypto_tickers_ticker_get**](docs/CurrenciesCryptoApi.md#v2_snapshot_locale_global_markets_crypto_tickers_ticker_get) | **GET** /v2/snapshot/locale/global/markets/crypto/tickers/{ticker} | Snapshot - Ticker
*CurrenciesForexApi* | [**v1_conversion_from_to_get**](docs/CurrenciesForexApi.md#v1_conversion_from_to_get) | **GET** /v1/conversion/{from}/{to} | Real-time Currency Conversion
*CurrenciesForexApi* | [**v1_historic_forex_from_to_date_get**](docs/CurrenciesForexApi.md#v1_historic_forex_from_to_date_get) | **GET** /v1/historic/forex/{from}/{to}/{date} | Historic Forex Ticks
*CurrenciesForexApi* | [**v1_last_quote_currencies_from_to_get**](docs/CurrenciesForexApi.md#v1_last_quote_currencies_from_to_get) | **GET** /v1/last_quote/currencies/{from}/{to} | Last Quote for a Currency Pair
*CurrenciesForexApi* | [**v2_aggs_ticker_forex_ticker_prev_get**](docs/CurrenciesForexApi.md#v2_aggs_ticker_forex_ticker_prev_get) | **GET** /v2/aggs/ticker/{forexTicker}/prev | Previous Close
*CurrenciesForexApi* | [**v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get**](docs/CurrenciesForexApi.md#v2_aggs_ticker_forex_ticker_range_multiplier_timespan_from_to_get) | **GET** /v2/aggs/ticker/{forexTicker}/range/{multiplier}/{timespan}/{from}/{to} | Aggregates (Bars)
*CurrenciesForexApi* | [**v2_snapshot_locale_global_markets_forex_direction_get**](docs/CurrenciesForexApi.md#v2_snapshot_locale_global_markets_forex_direction_get) | **GET** /v2/snapshot/locale/global/markets/forex/{direction} | Snapshot - Gainers/Losers
*CurrenciesForexApi* | [**v2_snapshot_locale_global_markets_forex_tickers_get**](docs/CurrenciesForexApi.md#v2_snapshot_locale_global_markets_forex_tickers_get) | **GET** /v2/snapshot/locale/global/markets/forex/tickers | Snapshot - All Tickers
*CurrenciesForexApi* | [**v2_snapshot_locale_global_markets_forex_tickers_ticker_get**](docs/CurrenciesForexApi.md#v2_snapshot_locale_global_markets_forex_tickers_ticker_get) | **GET** /v2/snapshot/locale/global/markets/forex/tickers/{ticker} | Snapshot - Ticker
*ReferenceApi* | [**v1_marketstatus_now_get**](docs/ReferenceApi.md#v1_marketstatus_now_get) | **GET** /v1/marketstatus/now | Market Status
*ReferenceApi* | [**v1_marketstatus_upcoming_get**](docs/ReferenceApi.md#v1_marketstatus_upcoming_get) | **GET** /v1/marketstatus/upcoming | Market Holidays
*ReferenceApi* | [**v1_meta_symbols_stocks_ticker_company_get**](docs/ReferenceApi.md#v1_meta_symbols_stocks_ticker_company_get) | **GET** /v1/meta/symbols/{stocksTicker}/company | Ticker Details
*ReferenceApi* | [**v1_meta_symbols_stocks_ticker_news_get**](docs/ReferenceApi.md#v1_meta_symbols_stocks_ticker_news_get) | **GET** /v1/meta/symbols/{stocksTicker}/news | Ticker News
*ReferenceApi* | [**v2_reference_dividends_stocks_ticker_get**](docs/ReferenceApi.md#v2_reference_dividends_stocks_ticker_get) | **GET** /v2/reference/dividends/{stocksTicker} | Stock Dividends
*ReferenceApi* | [**v2_reference_financials_stocks_ticker_get**](docs/ReferenceApi.md#v2_reference_financials_stocks_ticker_get) | **GET** /v2/reference/financials/{stocksTicker} | Stock Financials
*ReferenceApi* | [**v2_reference_locales_get**](docs/ReferenceApi.md#v2_reference_locales_get) | **GET** /v2/reference/locales | Locales
*ReferenceApi* | [**v2_reference_markets_get**](docs/ReferenceApi.md#v2_reference_markets_get) | **GET** /v2/reference/markets | Markets
*ReferenceApi* | [**v2_reference_splits_stocks_ticker_get**](docs/ReferenceApi.md#v2_reference_splits_stocks_ticker_get) | **GET** /v2/reference/splits/{stocksTicker} | Stock Splits
*ReferenceApi* | [**v2_reference_tickers_get**](docs/ReferenceApi.md#v2_reference_tickers_get) | **GET** /v2/reference/tickers | Tickers
*ReferenceApi* | [**v2_reference_types_get**](docs/ReferenceApi.md#v2_reference_types_get) | **GET** /v2/reference/types | Ticker Types
*ReferenceApi* | [**v_x_reference_tickers_get**](docs/ReferenceApi.md#v_x_reference_tickers_get) | **GET** /vX/reference/tickers | Tickers vX
*ReferenceApi* | [**v_x_reference_tickers_ticker_get**](docs/ReferenceApi.md#v_x_reference_tickers_ticker_get) | **GET** /vX/reference/tickers/{ticker} | Ticker Details vX
*StocksEquitiesApi* | [**v1_last_quote_stocks_stocks_ticker_get**](docs/StocksEquitiesApi.md#v1_last_quote_stocks_stocks_ticker_get) | **GET** /v1/last_quote/stocks/{stocksTicker} | Last Quote for a Symbol
*StocksEquitiesApi* | [**v1_last_stocks_stocks_ticker_get**](docs/StocksEquitiesApi.md#v1_last_stocks_stocks_ticker_get) | **GET** /v1/last/stocks/{stocksTicker} | Last Trade for a Symbol
*StocksEquitiesApi* | [**v1_meta_conditions_ticktype_get**](docs/StocksEquitiesApi.md#v1_meta_conditions_ticktype_get) | **GET** /v1/meta/conditions/{ticktype} | Condition Mappings
*StocksEquitiesApi* | [**v1_meta_exchanges_get**](docs/StocksEquitiesApi.md#v1_meta_exchanges_get) | **GET** /v1/meta/exchanges | Stock Exchanges
*StocksEquitiesApi* | [**v1_open_close_stocks_ticker_date_get**](docs/StocksEquitiesApi.md#v1_open_close_stocks_ticker_date_get) | **GET** /v1/open-close/{stocksTicker}/{date} | Daily Open/Close
*StocksEquitiesApi* | [**v2_aggs_grouped_locale_us_market_stocks_date_get**](docs/StocksEquitiesApi.md#v2_aggs_grouped_locale_us_market_stocks_date_get) | **GET** /v2/aggs/grouped/locale/us/market/stocks/{date} | Grouped Daily (Bars)
*StocksEquitiesApi* | [**v2_aggs_ticker_stocks_ticker_prev_get**](docs/StocksEquitiesApi.md#v2_aggs_ticker_stocks_ticker_prev_get) | **GET** /v2/aggs/ticker/{stocksTicker}/prev | Previous Close
*StocksEquitiesApi* | [**v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get**](docs/StocksEquitiesApi.md#v2_aggs_ticker_stocks_ticker_range_multiplier_timespan_from_to_get) | **GET** /v2/aggs/ticker/{stocksTicker}/range/{multiplier}/{timespan}/{from}/{to} | Aggregates (Bars)
*StocksEquitiesApi* | [**v2_last_nbbo_stocks_ticker_get**](docs/StocksEquitiesApi.md#v2_last_nbbo_stocks_ticker_get) | **GET** /v2/last/nbbo/{stocksTicker} | Last NBBO for a Symbol
*StocksEquitiesApi* | [**v2_last_trade_stocks_ticker_get**](docs/StocksEquitiesApi.md#v2_last_trade_stocks_ticker_get) | **GET** /v2/last/trade/{stocksTicker} | Last Trade for a Symbol
*StocksEquitiesApi* | [**v2_snapshot_locale_us_markets_stocks_direction_get**](docs/StocksEquitiesApi.md#v2_snapshot_locale_us_markets_stocks_direction_get) | **GET** /v2/snapshot/locale/us/markets/stocks/{direction} | Snapshot - Gainers/Losers
*StocksEquitiesApi* | [**v2_snapshot_locale_us_markets_stocks_tickers_get**](docs/StocksEquitiesApi.md#v2_snapshot_locale_us_markets_stocks_tickers_get) | **GET** /v2/snapshot/locale/us/markets/stocks/tickers | Snapshot - All Tickers
*StocksEquitiesApi* | [**v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get**](docs/StocksEquitiesApi.md#v2_snapshot_locale_us_markets_stocks_tickers_stocks_ticker_get) | **GET** /v2/snapshot/locale/us/markets/stocks/tickers/{stocksTicker} | Snapshot - Ticker
*StocksEquitiesApi* | [**v2_ticks_stocks_nbbo_ticker_date_get**](docs/StocksEquitiesApi.md#v2_ticks_stocks_nbbo_ticker_date_get) | **GET** /v2/ticks/stocks/nbbo/{ticker}/{date} | Quotes (NBBO)
*StocksEquitiesApi* | [**v2_ticks_stocks_trades_ticker_date_get**](docs/StocksEquitiesApi.md#v2_ticks_stocks_trades_ticker_date_get) | **GET** /v2/ticks/stocks/trades/{ticker}/{date} | Trades


## Documentation For Models

 - [AskExchangeId](docs/AskExchangeId.md)
 - [BidExchangeId](docs/BidExchangeId.md)
 - [Company](docs/Company.md)
 - [ConditionTypeMap](docs/ConditionTypeMap.md)
 - [Conditions](docs/Conditions.md)
 - [CryptoExchange](docs/CryptoExchange.md)
 - [CryptoGroupedResults](docs/CryptoGroupedResults.md)
 - [CryptoGroupedResultsResults](docs/CryptoGroupedResultsResults.md)
 - [CryptoHistoricTrades](docs/CryptoHistoricTrades.md)
 - [CryptoLastTrade](docs/CryptoLastTrade.md)
 - [CryptoLastTradeLast](docs/CryptoLastTradeLast.md)
 - [CryptoOpenClose](docs/CryptoOpenClose.md)
 - [CryptoSnapshotMinute](docs/CryptoSnapshotMinute.md)
 - [CryptoSnapshotTicker](docs/CryptoSnapshotTicker.md)
 - [CryptoSnapshotTickerFullBook](docs/CryptoSnapshotTickerFullBook.md)
 - [CryptoSnapshotTickerFullBookBids](docs/CryptoSnapshotTickerFullBookBids.md)
 - [CryptoSnapshotTickerTicker](docs/CryptoSnapshotTickerTicker.md)
 - [CryptoSnapshotTickerTickerMin](docs/CryptoSnapshotTickerTickerMin.md)
 - [CryptoSnapshotTickers](docs/CryptoSnapshotTickers.md)
 - [CryptoTick](docs/CryptoTick.md)
 - [Dividend](docs/Dividend.md)
 - [DividendResults](docs/DividendResults.md)
 - [Exchange](docs/Exchange.md)
 - [Financial](docs/Financial.md)
 - [Financials](docs/Financials.md)
 - [ForexConversion](docs/ForexConversion.md)
 - [ForexConversionLast](docs/ForexConversionLast.md)
 - [ForexGroupedResults](docs/ForexGroupedResults.md)
 - [ForexGroupedResultsResults](docs/ForexGroupedResultsResults.md)
 - [ForexHistoricTrades](docs/ForexHistoricTrades.md)
 - [ForexHistoricTradesTicks](docs/ForexHistoricTradesTicks.md)
 - [ForexPairLastQuote](docs/ForexPairLastQuote.md)
 - [ForexPreviousClose](docs/ForexPreviousClose.md)
 - [ForexPreviousCloseResults](docs/ForexPreviousCloseResults.md)
 - [ForexSnapshotLastQuote](docs/ForexSnapshotLastQuote.md)
 - [ForexSnapshotPrevDay](docs/ForexSnapshotPrevDay.md)
 - [ForexSnapshotTicker](docs/ForexSnapshotTicker.md)
 - [ForexSnapshotTickerTicker](docs/ForexSnapshotTickerTicker.md)
 - [ForexSnapshotTickerTickerDay](docs/ForexSnapshotTickerTickerDay.md)
 - [ForexSnapshotTickerTickerLastQuote](docs/ForexSnapshotTickerTickerLastQuote.md)
 - [ForexSnapshotTickerTickerMin](docs/ForexSnapshotTickerTickerMin.md)
 - [ForexSnapshotTickers](docs/ForexSnapshotTickers.md)
 - [ForexTickerResults](docs/ForexTickerResults.md)
 - [ForexTickerResultsResults](docs/ForexTickerResultsResults.md)
 - [Indicators](docs/Indicators.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse20010](docs/InlineResponse20010.md)
 - [InlineResponse20010OpenTrades](docs/InlineResponse20010OpenTrades.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [InlineResponse2003Results](docs/InlineResponse2003Results.md)
 - [InlineResponse2004](docs/InlineResponse2004.md)
 - [InlineResponse2005](docs/InlineResponse2005.md)
 - [InlineResponse2005Currencies](docs/InlineResponse2005Currencies.md)
 - [InlineResponse2005Exchanges](docs/InlineResponse2005Exchanges.md)
 - [InlineResponse2006](docs/InlineResponse2006.md)
 - [InlineResponse2007](docs/InlineResponse2007.md)
 - [InlineResponse2008](docs/InlineResponse2008.md)
 - [InlineResponse2009](docs/InlineResponse2009.md)
 - [InlineResponse200Attrs](docs/InlineResponse200Attrs.md)
 - [InlineResponse200Codes](docs/InlineResponse200Codes.md)
 - [InlineResponse200Tickers](docs/InlineResponse200Tickers.md)
 - [Locales](docs/Locales.md)
 - [LocalesResults](docs/LocalesResults.md)
 - [Map](docs/Map.md)
 - [MapKey](docs/MapKey.md)
 - [MarketHoliday](docs/MarketHoliday.md)
 - [MarketStatus](docs/MarketStatus.md)
 - [Markets](docs/Markets.md)
 - [MarketsResults](docs/MarketsResults.md)
 - [News](docs/News.md)
 - [PaginationHooksBase](docs/PaginationHooksBase.md)
 - [RatingSection](docs/RatingSection.md)
 - [RequestIdBase](docs/RequestIdBase.md)
 - [SnapshotLastTrade](docs/SnapshotLastTrade.md)
 - [SnapshotOHLCV](docs/SnapshotOHLCV.md)
 - [SnapshotOHLCVVW](docs/SnapshotOHLCVVW.md)
 - [Split](docs/Split.md)
 - [SplitResults](docs/SplitResults.md)
 - [StandardBase](docs/StandardBase.md)
 - [StandardBaseAllOf](docs/StandardBaseAllOf.md)
 - [StandardBaseAllOf1](docs/StandardBaseAllOf1.md)
 - [StatusBase](docs/StatusBase.md)
 - [StatusCountBase](docs/StatusCountBase.md)
 - [StocksGroupedResults](docs/StocksGroupedResults.md)
 - [StocksGroupedResultsResults](docs/StocksGroupedResultsResults.md)
 - [StocksLastQuote](docs/StocksLastQuote.md)
 - [StocksLastQuoteLast](docs/StocksLastQuoteLast.md)
 - [StocksLastTrade](docs/StocksLastTrade.md)
 - [StocksLastTradeLast](docs/StocksLastTradeLast.md)
 - [StocksOpenClose](docs/StocksOpenClose.md)
 - [StocksSnapshotLastQuote](docs/StocksSnapshotLastQuote.md)
 - [StocksSnapshotMinute](docs/StocksSnapshotMinute.md)
 - [StocksSnapshotTicker](docs/StocksSnapshotTicker.md)
 - [StocksSnapshotTickers](docs/StocksSnapshotTickers.md)
 - [StocksSnapshotTickersDay](docs/StocksSnapshotTickersDay.md)
 - [StocksSnapshotTickersLastQuote](docs/StocksSnapshotTickersLastQuote.md)
 - [StocksSnapshotTickersLastTrade](docs/StocksSnapshotTickersLastTrade.md)
 - [StocksSnapshotTickersMin](docs/StocksSnapshotTickersMin.md)
 - [StocksSnapshotTickersTickers](docs/StocksSnapshotTickersTickers.md)
 - [StocksV2Base](docs/StocksV2Base.md)
 - [StocksV2NBBO](docs/StocksV2NBBO.md)
 - [StocksV2NBBOAllOf](docs/StocksV2NBBOAllOf.md)
 - [StocksV2NBBOs](docs/StocksV2NBBOs.md)
 - [StocksV2Trade](docs/StocksV2Trade.md)
 - [StocksV2TradeAllOf](docs/StocksV2TradeAllOf.md)
 - [StocksV2TradeAllOf1](docs/StocksV2TradeAllOf1.md)
 - [StocksV2Trades](docs/StocksV2Trades.md)
 - [TickerBase](docs/TickerBase.md)
 - [TickerResults](docs/TickerResults.md)
 - [TickerResultsResults](docs/TickerResultsResults.md)
 - [TickerTypes](docs/TickerTypes.md)
 - [TickerTypesResults](docs/TickerTypesResults.md)
 - [TickerTypesResultsIndexTypes](docs/TickerTypesResultsIndexTypes.md)
 - [TickerTypesResultsTypes](docs/TickerTypesResultsTypes.md)
 - [Tickers](docs/Tickers.md)
 - [TradeDetailsMapItem](docs/TradeDetailsMapItem.md)
 - [V1LastBase](docs/V1LastBase.md)
 - [V2AggsBase](docs/V2AggsBase.md)
 - [V2LastBase](docs/V2LastBase.md)
 - [V2TicksBase](docs/V2TicksBase.md)
 - [VXTickerDetails](docs/VXTickerDetails.md)
 - [VXTickerDetailsResults](docs/VXTickerDetailsResults.md)
 - [VXTickerDetailsResultsAddress](docs/VXTickerDetailsResultsAddress.md)
 - [VXTickers](docs/VXTickers.md)
 - [VXTickersResults](docs/VXTickersResults.md)


## Documentation For Authorization


## apiKey

- **Type**: API key
- **API key parameter name**: apiKey
- **Location**: URL query string


## Author




## Notes for Large OpenAPI documents
If the OpenAPI document is large, imports in openapi_client.apis and openapi_client.models may fail with a
RecursionError indicating the maximum recursion limit has been exceeded. In that case, there are a couple of solutions:

Solution 1:
Use specific imports for apis and models like:
- `from openapi_client.api.default_api import DefaultApi`
- `from openapi_client.model.pet import Pet`

Solution 2:
Before importing the package, adjust the maximum recursion limit as shown below:
```
import sys
sys.setrecursionlimit(1500)
import openapi_client
from openapi_client.apis import *
from openapi_client.models import *
```

