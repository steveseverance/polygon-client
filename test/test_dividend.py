"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.dividend_results import DividendResults
globals()['DividendResults'] = DividendResults
from openapi_client.model.dividend import Dividend


class TestDividend(unittest.TestCase):
    """Dividend unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDividend(self):
        """Test Dividend"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Dividend()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
