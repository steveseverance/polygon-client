"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.vx_ticker_details_results import VXTickerDetailsResults
globals()['VXTickerDetailsResults'] = VXTickerDetailsResults
from openapi_client.model.vx_ticker_details import VXTickerDetails


class TestVXTickerDetails(unittest.TestCase):
    """VXTickerDetails unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testVXTickerDetails(self):
        """Test VXTickerDetails"""
        # FIXME: construct object with mandatory attributes with example values
        # model = VXTickerDetails()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
