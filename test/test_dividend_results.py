"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.dividend_results import DividendResults


class TestDividendResults(unittest.TestCase):
    """DividendResults unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDividendResults(self):
        """Test DividendResults"""
        # FIXME: construct object with mandatory attributes with example values
        # model = DividendResults()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
