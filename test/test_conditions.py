"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.conditions import Conditions


class TestConditions(unittest.TestCase):
    """Conditions unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testConditions(self):
        """Test Conditions"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Conditions()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
