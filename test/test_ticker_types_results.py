"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.ticker_types_results_index_types import TickerTypesResultsIndexTypes
from openapi_client.model.ticker_types_results_types import TickerTypesResultsTypes
globals()['TickerTypesResultsIndexTypes'] = TickerTypesResultsIndexTypes
globals()['TickerTypesResultsTypes'] = TickerTypesResultsTypes
from openapi_client.model.ticker_types_results import TickerTypesResults


class TestTickerTypesResults(unittest.TestCase):
    """TickerTypesResults unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTickerTypesResults(self):
        """Test TickerTypesResults"""
        # FIXME: construct object with mandatory attributes with example values
        # model = TickerTypesResults()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
