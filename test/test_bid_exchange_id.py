"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.bid_exchange_id import BidExchangeId


class TestBidExchangeId(unittest.TestCase):
    """BidExchangeId unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testBidExchangeId(self):
        """Test BidExchangeId"""
        # FIXME: construct object with mandatory attributes with example values
        # model = BidExchangeId()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
