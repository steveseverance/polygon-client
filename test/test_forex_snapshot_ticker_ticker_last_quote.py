"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.forex_snapshot_ticker_ticker_last_quote import ForexSnapshotTickerTickerLastQuote


class TestForexSnapshotTickerTickerLastQuote(unittest.TestCase):
    """ForexSnapshotTickerTickerLastQuote unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testForexSnapshotTickerTickerLastQuote(self):
        """Test ForexSnapshotTickerTickerLastQuote"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ForexSnapshotTickerTickerLastQuote()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
