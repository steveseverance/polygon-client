"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.stocks_v2_nbbo_all_of import StocksV2NBBOAllOf
from openapi_client.model.stocks_v2_trade_all_of import StocksV2TradeAllOf
globals()['StocksV2NBBOAllOf'] = StocksV2NBBOAllOf
globals()['StocksV2TradeAllOf'] = StocksV2TradeAllOf
from openapi_client.model.stocks_v2_nbbo import StocksV2NBBO


class TestStocksV2NBBO(unittest.TestCase):
    """StocksV2NBBO unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testStocksV2NBBO(self):
        """Test StocksV2NBBO"""
        # FIXME: construct object with mandatory attributes with example values
        # model = StocksV2NBBO()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
