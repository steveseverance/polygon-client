"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.stocks_last_trade_last import StocksLastTradeLast
globals()['StocksLastTradeLast'] = StocksLastTradeLast
from openapi_client.model.stocks_last_trade import StocksLastTrade


class TestStocksLastTrade(unittest.TestCase):
    """StocksLastTrade unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testStocksLastTrade(self):
        """Test StocksLastTrade"""
        # FIXME: construct object with mandatory attributes with example values
        # model = StocksLastTrade()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
