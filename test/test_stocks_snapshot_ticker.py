"""
    Polygon API

    The future of fintech.  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.stocks_snapshot_tickers_tickers import StocksSnapshotTickersTickers
globals()['StocksSnapshotTickersTickers'] = StocksSnapshotTickersTickers
from openapi_client.model.stocks_snapshot_ticker import StocksSnapshotTicker


class TestStocksSnapshotTicker(unittest.TestCase):
    """StocksSnapshotTicker unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testStocksSnapshotTicker(self):
        """Test StocksSnapshotTicker"""
        # FIXME: construct object with mandatory attributes with example values
        # model = StocksSnapshotTicker()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
